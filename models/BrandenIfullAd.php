<?php
/**
 * App首頁輪播廣告
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullAd extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("廣告啟用時間")
     *
     * @var string
     */
    public $start_at;

    /**
     * @Comment("廣告下架時間")
     *
     * @var string
     */
    public $end_at;

    /**
     * @Comment("是否啟用")
     *
     * @var integer
     */
    public $is_active;

    /**
     * @Comment("廣告標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("廣告連結網址")
     *
     * @var string
     */
    public $url;

    /**
     * @Comment("新增廣告時間")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_ad';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullAd[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullAd
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
