<?php
/**
 * 社區公告訊息紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullMsg extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯範本編號")
     *
     * @var integer
     */
    public $msg_lazy_id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯社區公告類型編號")
     *
     * @var integer
     */
    public $cmn_msg_type_id;

    /**
     * @Comment("訊息有效/活動開始時間")
     *
     * @var string
     */
    public $start_at;

    /**
     * @Comment("訊息失效/活動結束時間")
     *
     * @var string
     */
    public $end_at;

    /**
     * @Comment("是否發佈")
     *
     * @var integer
     */
    public $is_active;

    /**
     * @Comment("是否於公告置頂")
     *
     * @var integer
     */
    public $is_ontop;

    /**
     * @Comment("是否為活動")
     *
     * @var integer
     */
    public $is_event;

    /**
     * @Comment("標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("課程報名連結網址欄位")
     *
     * @var string
     */
    public $url;

    /**
     * @Comment("訊息")
     *
     * @var string
     */
    public $message;

    /**
     * @Comment("新增紀錄時間")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("修改紀錄時間")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgRead', 'msg_id', array('alias' => 'BrandenIfullMsgRead'));
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgBlock', 'msg_id', array('alias' => 'BrandenIfullMsgBlock'));
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgUnit', 'msg_id', array('alias' => 'BrandenIfullMsgUnit'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('cmn_msg_type_id', '\Ifulifeapi\Models\BrandenIfullCmnMsgType', 'id', array('alias' => 'BrandenIfullCmnMsgType'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_msg';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsg[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsg
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'msg_lazy_id' => 'msg_lazy_id',
            'cmt_id' => 'cmt_id',
            'cmn_msg_type_id' => 'cmn_msg_type_id',
            'start_at' => 'start_at',
            'end_at' => 'end_at',
            'is_active' => 'is_active',
            'is_ontop' => 'is_ontop',
            'is_event' => 'is_event',
            'title' => 'title',
            'url' => 'url',
            'message' => 'message',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
