<?php
/**
 * 社區公告訊息針對棟別紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullMsgBlock extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區公告編號")
     *
     * @var integer
     */
    public $msg_id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯棟別編號")
     *
     * @var integer
     */
    public $cmt_block_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('msg_id', '\Ifulifeapi\Models\BrandenIfullMpg', 'id', array('alias' => 'BrandenIfullMpg'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('cmt_block_id', '\Ifulifeapi\Models\BrandenIfullCmtBlock', 'id', array('alias' => 'BrandenIfullCmtBlock'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_msg_block';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsgBlock[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsgBlock
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'msg_id' => 'msg_id',
            'cmt_id' => 'cmt_id',
            'cmt_block_id' => 'cmt_block_id',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
