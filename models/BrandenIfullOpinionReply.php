<?php
/**
 * 意見提出回覆紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullOpinionReply extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯意見編號")
     *
     * @var integer
     */
    public $opinion_id;

    /**
     * @Comment("關聯管理帳號(回覆者)")
     *
     * @var integer
     */
    public $backend_users_id;

    /**
     * @Comment("意見回覆時間(created_at)")
     *
     * @var string
     */
    public $reply_date;

    /**
     * @Comment("回覆內容")
     *
     * @var string
     */
    public $reply;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $quick_reply;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('opinion_id', '\Ifulifeapi\Models\BrandenIfullOpinion', 'id', array('alias' => 'BrandenIfullOpinion'));
        $this->belongsTo('backend_users_id', '\Ifulifeapi\Models\BackendUsers', 'id', array('alias' => 'BackendUsers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_opinion_reply';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullOpinionReply[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullOpinionReply
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'opinion_id' => 'opinion_id',
            'backend_users_id' => 'backend_users_id',
            'reply_date' => 'reply_date',
            'reply' => 'reply',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'quick_reply' => 'quick_reply'
        );
    }

}
