<?php

namespace Ifulifeapi\Models;

use Phalcon\DI;
use \Personalwork\Mvc\Model as PersonalworkModel;

class SystemFiles extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("為了最佳化索引與儲存效能，會取disk_name以3碼為單位，配置3層次目錄")
     *
     * @var string
     */
    public $disk_name;

    /**
     * @Comment("顯示檔案名稱。")
     *
     * @var string
     */
    public $file_name;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $file_size;

    /**
     * @Comment("mime檔案類型。")
     *
     * @var string
     */
    public $content_type;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $description;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $field;

    /**
     * @Comment("代表根據attachment_type找到的table primary_id數值，即為關聯主鍵。")
     *
     * @var string
     */
    public $attachment_id;

    /**
     * @Comment("代表新增class model透過轉換"/"為_並全部字母小寫化的方式可以找到相對應的relation table")
     *
     * @var string
     */
    public $attachment_type;

    /**
     * @Comment("為1才顯示/啟用。")
     *
     * @var integer
     */
    public $is_public;

    /**
     * @Comment("排序值，依照ASC排序。")
     *
     * @var integer
     */
    public $sort_order;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;


    protected static $attachmentTypeMapping = [
        // 社區公告
        'Ifulifeapi\Models\BrandenIfullMsg' => "Branden\\iFull\\Models\\Msg",
        // 意見提出
        'Ifulifeapi\Models\Opinion' => "Branden\\iFull\\Models\\Opinion",
        // 公設預約
        'Ifulifeapi\Models\Utilities' => "Branden\\iFull\\Models\\Utilities",
        // 首頁廣告Banner
        'Ifulifeapi\Models\BrandenIfullAd' => "Branden\\iFull\\Models\\Ad"

    ];

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'system_files';
    }


    /**
     * 直接調整使用$parameters參數格式，以下述呼叫方式處理
     *
     * $files = Ifulifeapi\Models\SystemFiles::find([
        'attachmentType' => 'Ifulifeapi\Models\BrandenIfullMsg',
        'attachmentId' => '4'
      ]);
     * */
    protected static function _appendAttachment(&$parameters = null) {
        if( array_key_exists($parameters['attachmentType'], self::$attachmentTypeMapping) ){
            $parameters = [
                'conditions' => 'attachment_type = :T: AND attachment_id = :I: AND is_public=1',
                // 'conditions' => 'attachment_id = :I:',
                'bind' => [
                    'T'=> self::$attachmentTypeMapping[$parameters['attachmentType']],
                    'I'=> (string)$parameters['attachmentId']
                ],
                'order' => 'sort_order ASC'
            ];
            unset($parameters['attachmentType']);
            unset($parameters['attachmentId']);
        }
        // Branden\iFull\Models\Msg

        return $parameters;
    }


    /**
     * 附加檔案參數後回傳
     * @param filepath 檔案絕對路徑
     * @param accessurl 檔案網址
     * */
    protected static function _result(&$res) {
        $config = DI::getDefault()->get('config')->octobercms;

        $data = $res->filter(function($row) use ($config){
            $rowdata = $row->toArray();

            $dir = array();
            $dir[] = substr($row->disk_name, 0 ,3);
            $dir[] = substr($row->disk_name, 3 ,3);
            $dir[] = substr($row->disk_name, 6 ,3);
            $postfixpath = implode('/', $dir).'/'.$row->disk_name;

            $rowdata['filepath'] = realpath($config->upload_path).'/'.$postfixpath;
            $rowdata['accessurl'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$config->prefix_url.$postfixpath;

            return $rowdata;
        });

        return $data;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemFiles[]
     */
    public static function find($parameters = null)
    {
        $parameters = self::_appendAttachment($parameters);

        return self::_result( parent::find($parameters) );
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemFiles
     */
    public static function findFirst($parameters = null)
    {
        $parameters = self::_appendAttachment($parameters);

        return self::_result( parent::find($parameters) );
    }


    public static function getAttachmentTypeMapping($key) {
        return self::$attachmentTypeMapping[$key];
    }


    /**
     * 處理檔案上傳到octbercms配置路徑！
     *
     * @param array $file $_FILES['photo']
     *
     * @return void
     */
    public function uploadToOctoberPath($file)
    {
        $config = DI::getDefault()->get('config')->octobercms;

        $ext = explode('.', $file['name']);
        // echo $file['haseVal'].'<BR/>';
        $trimVal = preg_replace(['/[^0-9\w]+/s'], [''], $file['haseVal']);
        // echo $trimVal.'<BR/>';
        $setName = strtolower(substr(microtime(), -9) . substr($trimVal, -16) . '.' . $ext[1]);
        // echo $setName.'<BR/>';
        $prefixDir = implode('/', array_slice(str_split($setName, 3), 0, 3)) . '/';
        // echo $prefixDir . $setName . '<BR/>';
        $real_to_october_path = realpath($config->upload_path) . '/public/';
        $newFullPath = $real_to_october_path . $prefixDir . $setName;
        // 建立目錄
        if( !is_dir( $real_to_october_path . $prefixDir) ){
            mkdir( $real_to_october_path.$prefixDir,0777, true);
        }
        if( move_uploaded_file($file['tmp_name'], $newFullPath) ){
            $this->disk_name = $setName;
            $this->file_name = $file['name'];
            $this->file_size = $file['size'];
            $this->content_type = $file['type'];
            return true;
        }else{
            return false;
        }
    }
}
