<?php
/**
 * 住戶推播通知資訊表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmtHouseholdMemberDevices extends PersonalworkModel
{
    // 預設接收全部通知
    const NTYPE_DEFAULT = 'all';
    // 後端系統訊息
    const NTYPE_SYSTEM = 'system';
    //  所有最新消息
    const NTYPE_MSG = 'msg';
    // 包裹到了
    // 收件待領
    const NTYPE_MPG_RECEIVE = 'mpg_receive';
    // 已領件
    const NTYPE_MPG_TAKE = 'mpg_take';
    // 已退件
    const NTYPE_MPG_REJECT = 'mpg_reject';

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯住戶編號")
     *
     * @var integer
     */
    public $cmt_household_member_id;

    /**
     * @Comment("登錄裝置資訊")
     *
     * @var string
     */
    public $deviceinfo;

    /**
     * @Comment("對應FCM憑證")
     *
     * @var string
     */
    public $fcm_token;

    /**
     * @Comment("是否啟用推播")
     *
     * @var integer
     */
    public $is_enable;

    /**
     * @Comment("接收推播項目")
     *
     * @var string
     */
    public $noticetypes;


    public static $_ntypeLabel = [
        self::NTYPE_DEFAULT => "全部",
        self::NTYPE_SYSTEM => "管理系統訊息",
        self::NTYPE_MSG => "最新消息",
        self::NTYPE_MPG_RECEIVE => "包裹到了",
        self::NTYPE_MPG_TAKE => "包裹領件",
        self::NTYPE_MPG_REJECT => "包裹退件",
    ];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('member_id', 'BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'BrandenIfullCmtHouseholdMember'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmt_household_member_devices';
    }


    public static function getNtypeLabel($ntype=null) {
        if( !$ntype ){
            return self::$_ntypeLabel;
        }else{
            return self::$_ntypeLabel[$ntype];
        }
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHouseholdMemberDevices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHouseholdMemberDevices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_household_member_id' => 'cmt_household_member_id',
            'deviceinfo' => 'deviceinfo',
            'fcm_token' => 'fcm_token',
            'is_enable' => 'is_enable',
            'noticetypes' => 'noticetypes'
        );
    }
}
