<?php
/**
 * (同居人)住戶帳號資訊表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmtHouseholdMember extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯屋主編號")
     *
     * @var integer
     */
    public $cmt_household_id;

    /**
     * @Comment("使用者帳號")
     *
     * @var string
     */
    public $name;

    /**
     * @Comment("性別")
     *
     * @var string
     */
    public $sex;

    /**
     * @Comment("生日")
     *
     * @var string
     */
    public $birthday;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $relation;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $profession;

    /**
     * @Comment("聯絡電話")
     *
     * @var string
     */
    public $phone;

    /**
     * @Comment("手機門號(帳號)")
     *
     * @var string
     */
    public $mobile;

    /**
     * @Comment("開通碼")
     *
     * @var string
     */
    public $activation_code;

    /**
     * @Comment("是否帳號已經開通過")
     *
     * @var string
     */
    public $is_active;


    /**
     * @Comment("帳號開通時間紀錄")
     *
     * @var string
     */
    public $active_at;

    /**
     * @Comment("註冊密碼")
     *
     * @var string
     */
    public $password;

    /**
     * @Comment("是否為私人領件")
     *
     * @var string
     */
    public $is_private;

    /**
     * @Comment("是否為私人領件")
     *
     * @var string
     */
    public $is_agree;

    /**
     * @Comment("從AWS使用SNS服務註冊的裝置識別碼")
     *
     * @var string
     */
    public $fcm_token;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgRead', 'cmt_household_member_id', array('alias' => 'BrandenIfullMsgRead'));
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullOpinion', 'cmt_household_member_id', array('alias' => 'BrandenIfullOpinion'));
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMemberDevices', 'cmt_household_member_id', array('alias' => 'BrandenIfullCmtHouseholdMemberDevices'));
        $this->belongsTo('cmt_household_id', '\Ifulifeapi\Models\BrandenIfullCmtHousehold', 'id', array('alias' => 'BrandenIfullCmtHousehold'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmt_household_member';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHouseholdMember[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHouseholdMember
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_household_id' => 'cmt_household_id',
            'name' => 'name',
            'sex' => 'sex',
            'birthday' => 'birthday',
            'relation' => 'relation',
            'profession' => 'profession',
            'phone' => 'phone',
            'mobile' => 'mobile',
            'password' => 'password',
            'activation_code' => 'activation_code',
            'is_private' => 'is_private',
            'is_agree' => 'is_agree',
            'is_active' => 'is_active',
            'active_at' => 'active_at',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'fcm_token' => 'fcm_token'
        );
    }

}
