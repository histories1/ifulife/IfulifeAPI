<?php
/**
 * 社區後端系統物業管理帳號資訊表
 * 目前與[意見提出回覆紀錄表]有關聯
 * */

namespace Ifulifeapi\Models;

use Phalcon\Mvc\Model\Validator\Email as Email;use \Personalwork\Mvc\Model as PersonalworkModel;

class BackendUsers extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $first_name;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $last_name;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $login;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $email;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $password;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $activation_code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $persist_code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $reset_password_code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $permissions;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $is_activated;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $role_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $activated_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $last_login;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $is_superuser;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullOpinionReply', 'backend_users_id', array('alias' => 'BrandenIfullOpinionReply'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'backend_users';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BackendUsers[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BackendUsers
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'login' => 'login',
            'email' => 'email',
            'password' => 'password',
            'activation_code' => 'activation_code',
            'persist_code' => 'persist_code',
            'reset_password_code' => 'reset_password_code',
            'permissions' => 'permissions',
            'is_activated' => 'is_activated',
            'role_id' => 'role_id',
            'activated_at' => 'activated_at',
            'last_login' => 'last_login',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'is_superuser' => 'is_superuser'
        );
    }

}
