<?php
/**
 * 名詞定義資訊表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmnDefineNoun extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯名詞定義")
     *
     * @var integer
     */
    public $cmn_define_id;

    /**
     * @Comment("次類別代碼")
     *
     * @var string
     */
    public $noun_code;

    /**
     * @Comment("次類別名稱")
     *
     * @var string
     */
    public $noun;

    /**
     * @Comment("次類別名稱縮寫")
     *
     * @var string
     */
    public $snoun;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $para01;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $para02;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $para03;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('cmn_define_id', '\Ifulifeapi\Models\BrandenIfullCmnDefine', 'id', array('alias' => 'BrandenIfullCmnDefine'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmn_define_noun';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnDefineNoun[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnDefineNoun
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmn_define_id' => 'cmn_define_id',
            'noun_code' => 'noun_code',
            'noun' => 'noun',
            'snoun' => 'snoun',
            'para01' => 'para01',
            'para02' => 'para02',
            'para03' => 'para03',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
