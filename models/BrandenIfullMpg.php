<?php
/**
 * 社區郵件包裹紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullMpg extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯戶別編號")
     *
     * @var integer
     */
    public $cmt_unit_id;

    /**
     * @Comment("關聯住戶編號")
     *
     * @var integer
     */
    public $cmt_household_member_id;

    /**
     * @Comment("包裹送達時間")
     *
     * @var string
     */
    public $receipt_at;

    /**
     * @Comment("關聯包裹類型編號")
     *
     * @var integer
     */
    public $mpg_type_id;

    /**
     * @Comment("物流業者")
     *
     * @var integer
     */
    public $logistics_id;

    /**
     * @Comment("包裹條碼")
     *
     * @var string
     */
    public $barcode;

    /**
     * @Comment("包裹編號(社區自訂)")
     *
     * @var string
     */
    public $mpgcode;

    /**
     * @Comment("關聯包裹所在位置編號")
     *
     * @var integer
     */
    public $mpg_location_id;


    /**
     * @Comment("郵件狀態")
     *
     * @var integer
     */
    public $status;

    /**
     * @Comment("取件時間")
     *
     * @var string
     */
    public $collar_at;

    /**
     * @Comment("取件住戶編號")
     *
     * @var integer
     */
    public $rcp_cmt_household_member_id;

    /**
     * @Comment("備註")
     *
     * @var string
     */
    public $remark;

    /**
     * @Comment("退件申請時間")
     *
     * @var string
     */
    public $return_apply_at;

    /**
     * @Comment("關聯申請退件住戶編號")
     *
     * @var string
     */
    public $return_cmt_household_member_id;

    /**
     * @Comment("取消退件申請時間")
     *
     * @var string
     */
    public $return_cancel_at;

    /**
     * @Comment("退件碼(給住戶)")
     *
     * @var string
     */
    public $return_code;

    /**
     * @Comment("取退件物流/對象")
     *
     * @var integer
     */
    public $return_logistics_id;

    /**
     * @Comment("退貨物流取件時間")
     *
     * @var string
     */
    public $retuen_collar_at;

    /**
     * @Comment("退件物流編號(物流業者給的存根聯)")
     *
     * @var string
     */
    public $return_logistics_code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * @Comment("電腦簽名用途")
     *
     * @var string
     */
    public $collar_signature;

    /**
     * @Comment("電腦簽名用途")
     *
     * @var string
     */
    public $return_logistics_signature;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgBlock', 'msg_id', array('alias' => 'BrandenIfullMsgBlock'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('cmt_unit_id', '\Ifulifeapi\Models\BrandenIfullCmtUnit', 'id', array('alias' => 'BrandenIfullCmtUnit'));
        $this->belongsTo('cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'OnwerMember'));
        $this->belongsTo('mpg_type_id', '\Ifulifeapi\Models\BrandenIfullMpgType', 'id', array('alias' => 'BrandenIfullMpgType'));
        $this->belongsTo('mpg_location_id', '\Ifulifeapi\Models\BrandenIfullMpgLocation', 'id', array('alias' => 'BrandenIfullMpgLocation'));
        $this->belongsTo('rcp_cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'RcpMember'));
        $this->belongsTo('return_cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'ReturnMember'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_mpg';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMpg[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMpg
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_id' => 'cmt_id',
            'cmt_unit_id' => 'cmt_unit_id',
            'cmt_household_member_id' => 'cmt_household_member_id',
            'receipt_at' => 'receipt_at',
            'mpg_type_id' => 'mpg_type_id',
            'logistics_id' => 'logistics_id',
            'barcode' => 'barcode',
            'mpgcode' => 'mpgcode',
            'mpg_location_id' => 'mpg_location_id',
            'status' => 'status',
            'collar_at' => 'collar_at',
            'rcp_cmt_household_member_id' => 'rcp_cmt_household_member_id',
            'remark' => 'remark',
            'return_cmt_household_member_id' => 'return_cmt_household_member_id',
            'return_apply_at' => 'return_apply_at',
            'return_cancel_at' => 'return_cancel_at',
            'return_code' => 'return_code',
            'return_logistics_id' => 'return_logistics_id',
            'retuen_collar_at' => 'retuen_collar_at',
            'return_logistics_code' => 'return_logistics_code',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'collar_signature' => 'collar_signature',
            'return_logistics_signature' => 'return_logistics_signature',
        );
    }

}
