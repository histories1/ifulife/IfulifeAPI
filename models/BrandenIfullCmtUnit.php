<?php
/**
 * (建案物件)社區戶別資訊表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmtUnit extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯棟別編號")
     *
     * @var integer
     */
    public $cmt_block_id;

    /**
     * @Comment("戶別")
     *
     * @var string
     */
    public $unit;

    /**
     * @Comment("關聯樓層編號")
     *
     * @var integer
     */
    public $floor_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmn_area_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmn_zone_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $address;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsgUnit', 'cmt_unit_id', array('alias' => 'BrandenIfullMsgUnit'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('floor_id', '\Ifulifeapi\Models\BrandenIfullCmnFloor', 'id', array('alias' => 'BrandenIfullCmnFloor'));
        $this->belongsTo('cmt_block_id', '\Ifulifeapi\Models\BrandenIfullCmtBlock', 'id', array('alias' => 'BrandenIfullCmtBlock'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmt_unit';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtUnit[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtUnit
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_id' => 'cmt_id',
            'cmt_block_id' => 'cmt_block_id',
            'unit' => 'unit',
            'floor_id' => 'floor_id',
            'cmn_area_id' => 'cmn_area_id',
            'cmn_zone_id' => 'cmn_zone_id',
            'address' => 'address',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
