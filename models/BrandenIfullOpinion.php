<?php
/**
 * 意見提出紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullOpinion extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯戶別編號")
     *
     * @var integer
     */
    public $cmt_unit_id;

    /**
     * @Comment("關聯屋主編號(可忽略)")
     *
     * @var integer
     */
    public $cmt_household_id;

    /**
     * @Comment("關聯住戶編號(提出者)")
     *
     * @var integer
     */
    public $cmt_household_member_id;

    /**
     * @Comment("意見編號")
     *
     * @var string
     */
    public $opinion_no;

    /**
     * @Comment("從define找define_noun對應")
     *
     * @var integer
     */
    public $opinion_type_id;

    /**
     * @Comment("意見提出時間(created_at)")
     *
     * @var string
     */
    public $opinion_date;

    /**
     * @Comment("意見內容")
     *
     * @var string
     */
    public $opinion;

    /**
     * @Comment("是否結案")
     *
     * @var integer
     */
    public $opinion_status;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;


    /**
     * @Comment("App回報時填寫裝置資訊")
     *
     * @var string
     */
    public $phone_model;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullOpinionReply', 'opinion_id', array('alias' => 'BrandenIfullOpinionReply'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'BrandenIfullCmtHouseholdMember'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_opinion';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullOpinion[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullOpinion
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_id' => 'cmt_id',
            'cmt_unit_id' => 'cmt_unit_id',
            'cmt_household_id' => 'cmt_household_id',
            'cmt_household_member_id' => 'cmt_household_member_id',
            'opinion_no' => 'opinion_no',
            'opinion_type_id' => 'opinion_type_id',
            'opinion_date' => 'opinion_date',
            'opinion' => 'opinion',
            'opinion_status' => 'opinion_status',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'phone_model' => 'phone_model'
        );
    }

}
