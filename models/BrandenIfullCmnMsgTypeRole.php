<?php
/**
 * 社區公告類型發佈權限關聯表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmnMsgTypeRole extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區公告訊息類型編號")
     *
     * @var integer
     */
    public $cmn_msg_type_id;

    /**
     * @Comment("關聯物業管理群組編號")
     *
     * @var integer
     */
    public $backend_user_role_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('cmn_msg_type_id', '\Ifulifeapi\Models\BrandenIfullCmnMsgType', 'id', array('alias' => 'BrandenIfullCmnMsgType'));
        $this->belongsTo('backend_user_role_id', '\Ifulifeapi\Models\BackendUserRoles', 'id', array('alias' => 'BackendUserRoles'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmn_msg_type_role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnMsgTypeRole[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnMsgTypeRole
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmn_msg_type_id' => 'cmn_msg_type_id',
            'backend_user_role_id' => 'backend_user_role_id',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
