<?php
/**
 * 社區後端系統物業管理群組資訊表
 * 目前與[社區公告類型發佈權限關聯表]有關聯
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BackendUserRoles extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $name;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $description;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $permissions;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $is_system;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullCmnMsgTypeRole', 'backend_user_role_id', array('alias' => 'BrandenIfullCmnMsgTypeRole'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'backend_user_roles';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BackendUserRoles[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BackendUserRoles
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'name' => 'name',
            'code' => 'code',
            'description' => 'description',
            'permissions' => 'permissions',
            'is_system' => 'is_system',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
