<?php
/**
 * 瓦斯抄表紀錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullGas extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯社區編號")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("關聯戶別編號")
     *
     * @var integer
     */
    public $cmt_unit_id;

    /**
     * @Comment("關聯所有權人編號")
     *
     * @var integer
     */
    public $cmt_household_id;

    /**
     * @Comment("關聯住戶編號")
     *
     * @var integer
     */
    public $cmt_household_member_id;

    /**
     * @Comment("瓦斯表紀錄年月,日固定01")
     *
     * @var string
     */
    public $year_month;

    /**
     * @Comment("紀錄度數")
     *
     * @var string
     */
    public $degree;

    /**
     * @Comment("產生紀錄時間")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("更新紀錄時間")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('cmt_unit_id', '\Ifulifeapi\Models\BrandenIfullCmtUnit', 'id', array('alias' => 'BrandenIfullCmtUnit'));
        $this->belongsTo('cmt_household_id', '\Ifulifeapi\Models\BrandenIfullCmtHousehold', 'id', array('alias' => 'BrandenIfullCmtHousehold'));
        $this->belongsTo('cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'BrandenIfullCmtHouseholdMember'));
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_gas';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullGas[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullGas
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'cmt_id' => 'cmt_id',
            'cmt_unit_id' => 'cmt_unit_id',
            'cmt_household_id' => 'cmt_household_id',
            'cmt_household_member_id' => 'cmt_household_member_id',
            'year_month' => 'year_month',
            'degree' => 'degree',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
