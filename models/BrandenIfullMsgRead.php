<?php
/**
 * 社區公告訊息已讀記錄表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullMsgRead extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("關聯意見提出編號")
     *
     * @var integer
     */
    public $msg_id;

    /**
     * @Comment("關聯住戶編號")
     *
     * @var integer
     */
    public $cmt_household_member_id;

    /**
     * @Comment("新增紀錄時間(已讀)")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('msg_id', '\Ifulifeapi\Models\BrandenIfullMsg', 'id', array('alias' => 'BrandenIfullMsg'));
        $this->belongsTo('cmt_household_member_id', '\Ifulifeapi\Models\BrandenIfullCmtHouseholdMember', 'id', array('alias' => 'BrandenIfullCmtHouseholdMember'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_msg_read';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsgRead[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullMsgRead
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'msg_id' => 'msg_id',
            'cmt_household_member_id' => 'cmt_household_member_id',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
