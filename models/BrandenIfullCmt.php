<?php
/**
 * (建案)社區資訊表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmt extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("建案社區名稱")
     *
     * @var string
     */
    public $community;

    /**
     * @Comment("建照資訊")
     *
     * @var string
     */
    public $license_no;

    /**
     * @Comment("建商")
     *
     * @var string
     */
    public $sponsor;

    /**
     * @Comment("建商負責人")
     *
     * @var string
     */
    public $sponsor_chairman;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $sponsor_area_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $sponsor_zone_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $sponsor_address;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $designer;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $designer_company;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $supervisor;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $supervisor_company;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $builder;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $builder_company;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $land_no;

    /**
     * @Comment("關聯縣市編號")
     *
     * @var integer
     */
    public $cmn_area_id;

    /**
     * @Comment("關聯行政區編號")
     *
     * @var integer
     */
    public $cmn_zone_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $address;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $use_zone_id;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $ld_arcade_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $ld_setback_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $ld_other_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $ld_total_area;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $above_floor;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $under_floor;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $buildings;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $blocks;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $units;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $coverage_ratio;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $area_ratio;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $build_type_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmn_structure_type_id;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $bd_svl_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $bd_total_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $bd_height;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $bd_arcade_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $bd_other_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $above_ars_area;

    /**
     * @Comment("")
     *
     * @var double
     */
    public $under_ars_area;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $in_above_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $in_under_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $out_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $in_above_reward_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $in_under_reward_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $out_reward_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $legal_park;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $actual_park;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $license_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $bd_license_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $bd_license_no;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $bd_start_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $bd_complete_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $public_building;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $deleted_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $remark;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmt';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmt[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmt
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'community' => 'community',
            'license_no' => 'license_no',
            'sponsor' => 'sponsor',
            'sponsor_chairman' => 'sponsor_chairman',
            'sponsor_area_id' => 'sponsor_area_id',
            'sponsor_zone_id' => 'sponsor_zone_id',
            'sponsor_address' => 'sponsor_address',
            'designer' => 'designer',
            'designer_company' => 'designer_company',
            'supervisor' => 'supervisor',
            'supervisor_company' => 'supervisor_company',
            'builder' => 'builder',
            'builder_company' => 'builder_company',
            'land_no' => 'land_no',
            'cmn_area_id' => 'cmn_area_id',
            'cmn_zone_id' => 'cmn_zone_id',
            'address' => 'address',
            'use_zone_id' => 'use_zone_id',
            'ld_arcade_area' => 'ld_arcade_area',
            'ld_setback_area' => 'ld_setback_area',
            'ld_other_area' => 'ld_other_area',
            'ld_total_area' => 'ld_total_area',
            'above_floor' => 'above_floor',
            'under_floor' => 'under_floor',
            'buildings' => 'buildings',
            'blocks' => 'blocks',
            'units' => 'units',
            'coverage_ratio' => 'coverage_ratio',
            'area_ratio' => 'area_ratio',
            'build_type_id' => 'build_type_id',
            'cmn_structure_type_id' => 'cmn_structure_type_id',
            'bd_svl_area' => 'bd_svl_area',
            'bd_total_area' => 'bd_total_area',
            'bd_height' => 'bd_height',
            'bd_arcade_area' => 'bd_arcade_area',
            'bd_other_area' => 'bd_other_area',
            'above_ars_area' => 'above_ars_area',
            'under_ars_area' => 'under_ars_area',
            'in_above_park' => 'in_above_park',
            'in_under_park' => 'in_under_park',
            'out_park' => 'out_park',
            'in_above_reward_park' => 'in_above_reward_park',
            'in_under_reward_park' => 'in_under_reward_park',
            'out_reward_park' => 'out_reward_park',
            'legal_park' => 'legal_park',
            'actual_park' => 'actual_park',
            'license_date' => 'license_date',
            'bd_license_date' => 'bd_license_date',
            'bd_license_no' => 'bd_license_no',
            'bd_start_date' => 'bd_start_date',
            'bd_complete_date' => 'bd_complete_date',
            'public_building' => 'public_building',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'deleted_at' => 'deleted_at',
            'remark' => 'remark'
        );
    }

}
