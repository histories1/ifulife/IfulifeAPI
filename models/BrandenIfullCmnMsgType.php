<?php
/**
 * 社區公告類型定義表
 * */

namespace Ifulifeapi\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmnMsgType extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("社區公告類型名稱")
     *
     * @var string
     */
    public $msg_type;

    /**
     * @Comment("社區公告類型名稱縮寫")
     *
     * @var string
     */
    public $msg_type_sname;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullCmnMsgTypeRole', 'cmn_msg_type_id', array('alias' => 'BrandenIfullCmnMsgTypeRole'));
        $this->hasMany('id', '\Ifulifeapi\Models\BrandenIfullMsg', 'cmn_msg_type_id', array('alias' => 'BrandenIfullMsg'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmn_msg_type';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnMsgType[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmnMsgType
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'msg_type' => 'msg_type',
            'msg_type_sname' => 'msg_type_sname',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        );
    }

}
