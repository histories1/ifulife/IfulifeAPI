<?php
/**
 * (所有權人)屋主資訊表
 * */

namespace Ifulifeapi\Models;

use Phalcon\Mvc\Model\Validator\Email as Email;use \Personalwork\Mvc\Model as PersonalworkModel;

class BrandenIfullCmtHousehold extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmt_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmt_unit_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $in_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $out_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $owner;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $sex;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $birthday;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $reg_area_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $reg_zone_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $reg_address;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $con_area_id;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $con_zone_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $con_address;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $phone;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $mobile;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $email;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $cmn_living_status_id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $living_status;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $rent_no;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $is_keep_a_pet;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $locker;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $remark;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $agent;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $agent_phone;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $closing_date;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $activation_code;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $created_at;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $updated_at;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('cmt_id', '\Ifulifeapi\Models\BrandenIfullCmt', 'id', array('alias' => 'BrandenIfullCmt'));
        $this->belongsTo('cmt_unit_id', '\Ifulifeapi\Models\BrandenIfullCmtUnit', 'id', array('alias' => 'BrandenIfullCmtUnit'));

        //在使用update時排除特定欄位
        $this->skipAttributesOnCreate(array('updated_at'));
        $this->skipAttributesOnUpdate(array('created_at'));
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branden_ifull_cmt_household';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHousehold[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandenIfullCmtHousehold
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
