<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 *
 * @todo
 * 1. 使用micro controller區分不同類型API(依照選單需求區分、將共用部分置放於此)
 * [Phalcon Documentation](https://docs.phalconphp.com/3.4/en/application-micro#controllers)
 */
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Http\Response;
use Phalcon\Events\Manager as EventsManager;

/**
 * Add your routes here
 */
$app->get('/', function () use ($app) {
  echo $app['view']->render('index');
});


/**
 * 會員條款
 * 隱私權政策
 */
$app->get('/agreement', function () use ($app) {
  echo $app['view']->render('agreement');
});
$app->get('/privacy', function () use ($app) {
  echo $app['view']->render('privacy');
});


/**
 * GET 取得未讀最新消息、未領件包裹數量
 */
$app->get('/mybadge/{cmtId}/{unitId}/{memberId}',
function ($cmtId, $unitId, $memberId) use ($app) {
  $response = new Response();
  $response->setContentType('application/json');

  // 取出未讀數量
  $params = [
    "cmt_id=:cmt: AND is_active=1",
    "bind" => [
      'cmt' => $cmtId,
    ]
  ];
  $newest = \Ifulifeapi\Models\BrandenIfullMsg::find($params)->filter(function($row) use ($memberId){
    if( !count($row->getBrandenIfullMsgRead(["cmt_household_member_id={$memberId}"])) ){
      return $row->toArray();
    }
  });


  //notice : 取出未讀狀態且屬於該戶別訊息
  $params = [
    "status=122 AND
     cmt_id=:cmt: AND
     cmt_unit_id=:unit:",
    "bind" => [ 'cmt' => $cmtId, 'unit' => $unitId]
  ];
  $mpgs = \Ifulifeapi\Models\BrandenIfullMpg::count($params);

  $datas = [
    'msgcount' => count($newest),
    'mpgcount' => $mpgs,
  ];

  $response->setStatusCode(200, 'OK');
  if( !isset($_GET['DEBUG']) ){
    $response->setContent(json_encode($datas));
    return $response;
  }else{
    $response->setContentType('text/html; charset=utf-8');
    echo '<h4>首頁Badge數字$datas</h4>';
    var_dump($datas);die();
    return ;
  }

});


/**
 * GET / 取得意見提出類別清單
 * @todo : 加入jwt檢查！
 * */
$app->get('/opinion/types', function () use ($app) {
  // 意見提出指定類型來自父類別=10
  $types = Ifulifeapi\Models\BrandenIfullCmnDefineNoun::find(["cmn_define_id=10 AND para01='active' "]);
  $datas = [];
  foreach($types as $type){
    $datas[] = [
      'parentId' => $type->cmn_define_id,
      //notice : 修正意見提出設定的意見類別使用主鍵
      'id' => $type->id,
      'label'=>$type->noun,
    ];
  }
  if (isset($_GET['DEBUG'])) {
    echo '<h4>從BrandenIfullCmnDefineNoun取出意見類別$types</h4>';
    var_dump( $types->toArray());
  }

  $response = new Response();

  $response->setStatusCode(200, 'OK');
  if (!isset($_GET['DEBUG'])) {
    $response->setContentType('application/json');

    $response->setContent(json_encode($datas));
    return $response;
  } else {
    $response->setContentType('text/html; charset=utf-8');

    echo '<h4>廣告資料結構$datas</h4>';
    var_dump($datas);
    die();
    return;
  }

  $response->setContent(json_encode($datas));
  return $response;
});

/**
 * mount Opinion(意見提出) collections API routes
 * */
$sns = new MicroCollection();
$sns->setHandler(new \Ifulifeapi\Controllers\OpinionController());
$sns->setPrefix('/opinion');
$sns->post('/form', 'form');
$sns->get('/list/{unitId}/{memberId}', 'list');
$sns->get('/record/{opinionId}', 'record');
$app->mount($sns);

/**
 * mount Gas(瓦斯抄表) collections API routes
 * */
$sns = new MicroCollection();
$sns->setHandler(new \Ifulifeapi\Controllers\GasController());
$sns->setPrefix('/gas');
$sns->post('/form', 'form');
$sns->get('/list/?{unitId}', 'list');
$app->mount($sns);

/**
 * mount SNS collections API routes
 * */
$sns = new MicroCollection();
$sns->setHandler(new \Ifulifeapi\Controllers\SnsController());
$sns->setPrefix('/sns');
$sns->post('/smslistbytopic', 'smsListByTopic');
$sns->post('/smssubscribe', 'smsSubscribe');
$sns->post('/smspublish', 'smsPublish');
$sns->delete('/smsunsubscript/?{phonenumber}', 'smsUnsubscript');
$app->mount($sns);

/**
 * mount Auth collections API routes
 * */
$authen = new MicroCollection();
$authen->setHandler(new \Ifulifeapi\Controllers\AuthenController());
$authen->setPrefix('/authen');
// 使用“？”在變數之前可以忽略該變數配置
$authen->get( '/identity/?{phonenumber}', 'identity');
$authen->post('/register', 'register');
$authen->post('/login', 'login');
$authen->post('/editpassword', 'editpassword');
$authen->post('/fcm', 'fcm');
$authen->post('/setPrivate', 'setPrivate');
$authen->post('/setAgree', 'setAgree');
$authen->put('/logout/{phonenumber}', 'logout');

$app->mount($authen);


/**
 * mount Msg(社區公告) collections API routes
 * */
$msg = new MicroCollection();
$msg->setHandler(new \Ifulifeapi\Controllers\MsgController());
$msg->setPrefix('/msg');
$msg->get('/newest/{unitId}', 'newest');
// 使用“？”在變數之前可以忽略該變數配置
$msg->get('/list/{unitId}/{memberId}/?{filter_type}', 'list');
$msg->get('/detail/?{msgid}', 'detail');
$msg->get('/calendar/{cmtId}/{datestart}_{dateend}', 'calendar');
$msg->get('/calendarlist/{cmtId}/{datestart}', 'calendarlist');
$msg->post('/setreaded', 'setreaded');
$app->mount($msg);
/**
 * mount Mpg(包裹到了) collections API routes
 * */
$mpg = new MicroCollection();
$mpg->setHandler(new \Ifulifeapi\Controllers\MpgController());
$mpg->setPrefix('/mpg');
// 使用“？”在變數之前可以忽略該變數配置
$mpg->get('/list/{unitId}/{memberId}', 'list');
$mpg->post('/reject', 'reject');

$app->mount($mpg);

/**
 * mount Admin(愛富管理) collections API routes
 * */
$adm = new MicroCollection();
$adm->setHandler(new \Ifulifeapi\Controllers\AdministratorController());
$adm->setPrefix('/adm');
$adm->post('/signin', 'signin');
$adm->put('/signout/{accout}', 'signout');
$adm->post('/signin', 'signin');
// 訪客登記
$adm->post('/signin', 'signin');
// 包裹領件
// 包裹收件



/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});


/**
 * Attach Event for Micro Application
 * @see
 * 1. [mon-partenaire-phalcon/BodyParser.php at master · baptistecosta/mon-partenaire-phalcon](https://github.com/baptistecosta/mon-partenaire-phalcon/blob/master/app/module/api/plugin/BodyParser.php)
 * 2. [Using events with micro applications - Discussion - Phalcon Framework](https://forum.phalconphp.com/discussion/2036/using-events-with-micro-applications)
 */
//Create a events manager
$eventManager = new EventsManager();
//Listen all the application events
$eventManager->attach('micro', function ($event, $app) {
  if ($event->getType() == 'beforeExecuteRoute') {
    $contentType = $app->request->getHeader('CONTENT_TYPE');
    switch ($contentType) {
      case 'application/json':
      case 'application/json;charset=UTF-8':
        $jsonRawBody = $app->request->getJsonRawBody(true);
        if ( $app->request->getRawBody() && !$jsonRawBody) {
          $exceptionInfo = [
            'code'=> 501,
            'codeType' => "ERROR_PARAM_FORMAT",
            'errMsg' => "Invalid JSON syntax for convert to \$_POST"
          ];
          $response = new Phalcon\Http\Response();
          $response->setStatusCode( $exceptionInfo['code'], $exceptionInfo['codeType']);
          $response->setContentType('application/json');
          $response->setContent(json_encode( $exceptionInfo));
          $response->send();
          exit(1);
        }
        $_POST = $jsonRawBody;
        break;
    }
  }
});
$app->setEventsManager($eventManager);