<?php
/**
 * 預設使用mbstring來判斷字元長度
 * @see https://github.com/phalcon/cphalcon/issues/971
 */
if( function_exists('mb_internal_encoding') )
    mb_internal_encoding("utf-8");

define('PPS_APP_PROJECTNAME', 'ifulifeapi');
define('PPS_APP_APPSPATH', realpath('..'));
/**
 * 讀取README.md抓取裡面最後一筆版本定義
 */
if( $fp=fopen(getcwd().'/../README.md','r') ){
    while ( ($line = fgets($fp)) !== false ) {
        if( preg_match("/^v\s([^\(]+)/",$line,$m) ){
            define('VERSION', $m[1]);
            break;
        }
    }
    if( !defined('VERSION') ){ define('VERSION','0.5.0'); }
}

/**
 * Include Services
 */
include PPS_APP_APPSPATH . '/config/services.php';
($DEBUG=new \Phalcon\Debug())->listen();

include PPS_APP_APPSPATH . '/config/loader.php';

/**
 * Starting the application
 * Assign service locator to the application
 */
$app = new \Phalcon\Mvc\Micro($di);
include PPS_APP_APPSPATH . '/app.php';

$app->handle();