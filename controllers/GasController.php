<?php
/**
 * 瓦斯抄表機制
 * 1. 瓦斯抄表表單紀錄
 * 2. 取得以社區戶別為單位所列抄表紀錄
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class GasController extends Controller
{
  /**
   * 使用 hook 檢測JWT資訊！
   * */
  public function initialize() {
  }


  /**
   * POST 取得包裹到了列表
   * @todo : 加入jwt驗證
   * @param string 公告類型
   *
   * @return json
   * */
  public function form()
  {
    $this->response->setContentType('application/json');

    $memberId = $this->request->getPost('memberId', 'int');
    $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);

    $year_month = $this->request->getPost('year_month', 'string');
    $gas = \Ifulifeapi\Models\BrandenIfullGas::findFirst([
      "cmt_unit_id=:unit: AND year_month=:ym:",
      "bind" => [
        'unit' => $member->BrandenIfullCmtHousehold->cmt_unit_id,
        'ym' => $year_month . '-01'
      ]
    ]);
    if (!$gas) {
      $gas = new \Ifulifeapi\Models\BrandenIfullGas();
      $gas->cmt_id = $member->BrandenIfullCmtHousehold->cmt_id;
      $gas->cmt_unit_id = $member->BrandenIfullCmtHousehold->cmt_unit_id;
      $gas->cmt_household_id = $member->BrandenIfullCmtHousehold->id;
      $gas->cmt_household_member_id = $memberId;
      $gas->year_month = $year_month . '-01';
      $gas->created_at = date('Y-m-d H:i:s');
      $act = '已新增';
    } else {
      $act = '已更新';
    }
    $gas->degree = $this->request->getPost('degree', 'string');
    $gas->updated_at = date('Y-m-d H:i:s');

    if (!$gas->save()) {
      $codeInfo = [
        'code' => 500,
        'codeType' => 'APPLICATION_ERROR',
        'errMsg' => '瓦斯抄表紀錄發生錯誤：' . implode(',', $gas->getMessages())
      ];
      $this->response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html;charset=UTF-8;');
        var_dump($codeInfo);
      } else {
        $this->response->setContent(json_encode($codeInfo));
        return $this->response->send();
      }
    }

    $data = [
      'id' => $gas->id,
      'msg' => "{$year_month}紀錄{$act}，可到抄表紀錄確認。"
    ];
    $this->response->setContent(json_encode($data));
    return $this->response->send();
  }


  /**
   * GET 取得以戶別為單位所有抄表紀錄
   * @param int (以住戶帳號所對應的)戶別編號
   *
   * @return json
   * */
  public function list($unitId)
  {
    $this->response->setContentType('application/json');

    if (!intval($unitId)) {
      $codeInfo = [
        'code' => 403,
        'codeType' => 'AUTHEN_NOTFOUND',
        'errMsg' => '未指定社區戶別，無法取得瓦斯抄表紀錄'
      ];
      $this->response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      $this->response->setContent(json_encode($codeInfo));
      return $this->response->send();
    }

    $gases = \Ifulifeapi\Models\BrandenIfullGas::find([
      "cmt_unit_id=:unit:",
      "bind" => [
        'unit' => $unitId,
      ],
      "order" => "year_month DESC"
    ]);
    if( !count($gases) ){
      $codeInfo = [
        'code' => 200,
        'codeType' => 'DATA_NOTFOUND',
        'msg' => '目前無任何抄表紀錄'
      ];
      $this->response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      $this->response->setContent(json_encode($codeInfo));
      return $this->response->send();
    }

    $datas = [];
    foreach ($gases as $gas) {
      $key = str_replace('-01', '', $gas->year_month);
      $datas[$key] = [
        'id' => $gas->id,
        'memberName' => $gas->BrandenIfullCmtHouseholdMember->name,
        'year_month' => $gas->year_month,
        'degree'  => $gas->degree,
        'updated_at' => $gas->updated_at,
      ];
    }

    $this->response->setStatusCode(200, 'OK');
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      $this->response->setContent(var_dump($datas));
      return $this->response->send();
    } else {
      $this->response->setContent(json_encode( $datas));
      return $this->response->send();
    }
  }
}