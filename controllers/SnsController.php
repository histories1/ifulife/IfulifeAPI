<?php
/**
 * 處理串接Aws SNS服務發送簡訊機制
 * 1. 提供訂閱(註冊發送對象)清單API
 * 2. 向特定主題訂閱Endpoint（sms的Endpoint即是E.164格式的手機門號）
 * 3. 對特定主題傳送訊息內容並執行動作
 * 4. 可對特定主題註銷特定Endpoint
 *
 * @todo 定義錯誤碼對照表以及定義常數
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;


class SnsController extends Controller
{
  /**
   * post
   * 提供簡訊主題訂閱者清單API
   *
   * @param string $topic
   * arn:aws:sns:ap-southeast-1:518532647604:ifulife_deviceactive
   * @return mixed
   * [
   *  {
   *    "awsId": "1bdb3d1b-4af9-4ae9-83ca-cf8ae5c12b35",
   *    "phonenumber": "0988569734"
   *  }
   * ]
   * */
  public function smsListByTopic()
  {
    $response = new Response();

    try {
      if (!($topic = $this->request->getPost('topic', 'string'))) {
        throw new \Exception("參數錯誤", 501);
      }
      $config = $this->config->aws->toArray();
      $SnSclient = new SnsClient($config);

      $topicArn ="{$config['arnprefix']}{$topic}";
      $result = $SnSclient->listSubscriptionsByTopic(['TopicArn'=>$topicArn]);
      $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      if( count( $result['Subscriptions']) ){
        $users=[];
      foreach ( $result['Subscriptions'] as $key => $Subscription) {
        $twNumberProto = $phoneUtil->parse($Subscription['Endpoint'], "TW");
        $users[]=[
          'phonenumber' => str_replace(' ', '', $phoneUtil->format($twNumberProto, \libphonenumber\PhoneNumberFormat::NATIONAL)),
          'SubscriptionArn' => $Subscription['SubscriptionArn'],
          'awsId' => str_replace("{$Subscription['TopicArn']}:", '',  $Subscription['SubscriptionArn'])
        ];
      }
      }

      if( $_GET['DEBUG'] ){
        $response->setContentType('text/html');
        var_dump( $users);
      }else{
        $response->setContentType('application/json');
      }

      $response->setStatusCode(200, 'OK');
      $response->setContent(json_encode( $users));
      return $response;
    } catch (\libphonenumber\NumberParseException $e) {
      $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent($e->getMessage());
      error_log($e);
    } catch (AwsException $e) {
      $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent($e->getMessage());
      error_log($e->getMessage());
    }catch (\Exception $e) {
      $response->setStatusCode(501);
      $response->setContentType('text/html');
      $response->setContent($e->getMessage());
      error_log($e->getMessage());
    }

    return false;
  }


  /**
   * post
   * 向特定主題訂閱Endpoint（sms的Endpoint即是E.164格式的手機門號）
   * 重複傳送門號不會重複產生新的awsId
   *
   * @param string $topic
   * @param mixed|string $endpoint
   *
   * @return result
   * */
  public function smsSubscribe()
  {
    $response = new Response();

    try {
      $numbers=[];
      if( is_array( $this->request->getPost('endpoint') ) ){
        $filter = new \Phalcon\Filter();
        foreach( $this->request->getPost('endpoint') as $num){
          $numbers[]= $filter->sanitize( $num,\Phalcon\Filter::FILTER_ALPHANUM);
        }
      }elseif( !($numbers[]=$this->request->getPost('endpoint', 'alphanum', null, true))){
        throw new \Exception("參數錯誤");
      }
      if( !($topic= $this->request->getPost('topic', 'string', null, true))){
        throw new \Exception("參數錯誤");
      }
      $config = $this->config->aws->toArray();
      $topicArn = "{$config['arnprefix']}{$topic}";

      $SnSclient = new SnsClient($config);

      //act1. 過濾取得門號格式，考慮1至多組情況使用[]處理
      $subscribeParams = [
        'Protocol' => 'sms',
        'ReturnSubscriptionArn' => true,
        'TopicArn' => $topicArn,
      ];
      $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      $result=[];
      foreach ($numbers as $key => $number) {
        $twNumber = $phoneUtil->parse( $number, "TW");
        if( !$phoneUtil->isValidNumber( $twNumber) ){
          throw new \Exception("參數錯誤，手機格式有問題:{$number}", 502);
        }
        //act2. 從BrandenIfullCmtHouseholdMember比對 phone
        if(!($member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirstByPhone($number)) ){
          throw new \Exception("參數錯誤，找不到用戶手機:{$number}", 503);
        }
        //act3. 門號格式轉換成E.164
        $subscribeParams['Endpoint'] = $phoneUtil->format($twNumber, \libphonenumber\PhoneNumberFormat::E164);
        //act4. 取得裝置識別碼寫回資料表
        $resp = $SnSclient->subscribe($subscribeParams);
        if( empty( $member->aws_device_id) ){
          $member->aws_device_id = $resp['SubscriptionArn'];
          $member->save();
        }
        $result[$key] = [
          'id' => $member->id,
          'name' => $member->name,
          'phone' => $member->phone,
          'aws_device_id' => $member->aws_device_id,
        ];
      }

      $response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $response->setContentType('text/html');
        $response->setContent("<pre>".print_r($result)."</pre>");
      } else {
        $response->setContentType('application/json');
      }
      $response->setContent(json_encode($result));
      return $response;
    } catch (\libphonenumber\NumberParseException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent( "<pre>" . print_r( $e->getMessage()) . "</pre>");
      error_log($e);
    } catch (AwsException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    } catch (\Exception $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    }

    return false;
  }


  /**
   * post
   * 對特定主題傳送訊息內容
   * @param string $topic
   * @param string $message
   *
   * @return result
   * */
  public function smsPublish()
  {
    $response = new Response();

    try {
      $numbers = [];
      if (is_array($this->request->getPost('endpoint'))) {
        $filter = new \Phalcon\Filter();
        foreach ($this->request->getPost('endpoint') as $num) {
          $numbers[] = $filter->sanitize($num, \Phalcon\Filter::FILTER_ALPHANUM);
        }
      } elseif (!($numbers[] = $this->request->getPost('endpoint', 'alphanum', null, true))) {
        throw new \Exception("參數錯誤");
      }
      if (!($topic = $this->request->getPost('topic', 'string', null, true))) {
        throw new \Exception("參數錯誤");
      }
      $config = $this->config->aws->toArray();
      $topicArn = "{$config['arnprefix']}{$topic}";

      $SnSclient = new SnsClient($config);

      //act1. 過濾取得門號格式，考慮1至多組情況使用[]處理
      $subscribeParams = [
        'Protocol' => 'sms',
        'ReturnSubscriptionArn' => true,
        'TopicArn' => $topicArn,
      ];
      $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      $result = [];
      foreach ($numbers as $key => $number) {
      }

      $response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $response->setContentType('text/html');
        $response->setContent("<pre>" . print_r($result) . "</pre>");
      } else {
        $response->setContentType('application/json');
      }
      $response->setContent(json_encode($result));
      return $response;
    } catch (\libphonenumber\NumberParseException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e);
    } catch (AwsException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    } catch (\Exception $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    }

    return false;
  }



  /**
   * put
   * 可對特定主題取消訂閱特定Endpoint
   * @param string $topic
   * @param string $endpoint
   *
   * @return result
   * */
  public function smsUnsubscript()
  {
    $response = new Response();

    try {
      $numbers = [];
      if (is_array($this->request->getPost('endpoint'))) {
        $filter = new \Phalcon\Filter();
        foreach ($this->request->getPost('endpoint') as $num) {
          $numbers[] = $filter->sanitize($num, \Phalcon\Filter::FILTER_ALPHANUM);
        }
      } elseif (!($numbers[] = $this->request->getPost('endpoint', 'alphanum', null, true))) {
        throw new \Exception("參數錯誤");
      }
      if (!($topic = $this->request->getPost('topic', 'string', null, true))) {
        throw new \Exception("參數錯誤");
      }
      $config = $this->config->aws->toArray();
      $topicArn = "{$config['arnprefix']}{$topic}";

      $SnSclient = new SnsClient($config);

      //act1. 過濾取得門號格式，考慮1至多組情況使用[]處理
      $subscribeParams = [
        'Protocol' => 'sms',
        'ReturnSubscriptionArn' => true,
        'TopicArn' => $topicArn,
      ];
      $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      $result = [];
      foreach ($numbers as $key => $number) { }

      $response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $response->setContentType('text/html');
        $response->setContent("<pre>" . print_r($result) . "</pre>");
      } else {
        $response->setContentType('application/json');
      }
      $response->setContent(json_encode($result));
      return $response;
    } catch (\libphonenumber\NumberParseException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e);
    } catch (AwsException $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    } catch (\Exception $e) {
      // $response->setStatusCode($e->getCode());
      $response->setContentType('text/html');
      $response->setContent("<pre>" . print_r($e->getMessage()) . "</pre>");
      error_log($e->getMessage());
    }

    return false;
  }
}
