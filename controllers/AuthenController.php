<?php
/**
 * 處理住戶開通認證機制
 * 1. 人工由於物管人員登入基本用戶資料建立 BrandenIfullCmtHouseholdMember
 * 2. 從App介面（首頁=>我的=>判斷若未開通[確認帳號開始使用]=>未登入[啟用裝置]）
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;
use Phalcon\Crypt;
class AuthenController extends Controller
{


  /**
   * post
   * 處理開通
   * 1. 對開通主題(ifulife_deviceactive)，註冊訂閱者然後發送訊息後馬上刪除訂閱
   * 2. 將產生的訂閱紀錄加到預設主題(ifulife)，確保註冊訂閱者產生的id一致
   *
   * @return json datas
   * */
  public function identity($phonenumber)
  {
    $this->response->setContentType('application/json');

    try {
      if (!intval( $phonenumber) ) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
        echo '<h4>傳遞的門號:$phonenumber</h4>'.$phonenumber;
      }

      //從BrandenIfullCmtHouseholdMember比對mobile欄位
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirstByMobile($phonenumber);
      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到住戶帳號，請確認輸入資料正確。');
        throw new $exc;
      }
      if ($_GET['DEBUG']) {
        echo '<h4>取得住戶資訊:＄member</h4>';
        var_dump($member->toArray());
      }

      // 代表已經開通過
      if ( $member->is_active ) {
        if( !$member->password ){
          $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_IDENTIFY_ACTIVED_NEED_PASSWORD;
          throw new \Personalwork\Exceptions\Exception($sCode);
        }else{
          $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_IDENTIFY_FINISH_ACTIVED;
          throw new \Personalwork\Exceptions\Exception($sCode);
        }
        if ($_GET['DEBUG']) {
          echo "<h4>認證狀態: {$sCode}</h4>";
        }
      }

      if( !$member->activation_code ){
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_IDENTIFY_FAIL;
        $exc = new \Personalwork\Exceptions\AuthExceptions( $sCode);
        $exc->setMessage('管理中心尚未設定您的開通碼，請告知後台人員協助處理。');
        throw new $exc;
      }

      //NOTICE : 直接取得開通碼以及開通碼顯示的QRcode網址
      $datas = [
        'id'          => $member->id,
        'phone'       => $member->mobile,
        'name'        => $member->name,
        'code'        => $member->activation_code,
        'qrcode_url'  => $this->config->jwtAuth->activation_url . $member->activation_code,
        'updated_at'  => $member->updated_at,
      ];

      if ($_GET['DEBUG']) {
        echo '<h4>回傳開通資訊:</h4>';
        var_dump($datas);
      }

      $this->response->setStatusCode(200, "OK");
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * post
   * 處理開通後註冊裝置與設定密碼機制
   * 1. 將新增密碼並配置awsID資訊寫入資料庫
   * 2. 處理取得完整住戶資訊回傳App
   *
   * @param int $id
   * @param string $password
   *
   * @return json datas
   * */
  public function register()
  {
    $this->response->setContentType('application/json');

    try {
      $memberId = $this->request->getPost('id', 'int');
      $dcpwd=base64_decode($this->request->getPost('password', 'string'));
      if (!intval($memberId) || !$dcpwd )  {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);
      if (!$member ) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage( '找不到帳號，請確認輸入資料正確。');
        throw $exc;
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      $config = $this->config->aws->toArray();
      if ($_GET['DEBUG']) {
        echo '<h4>目前配置的AWS參數:$config</h4>';
        var_dump($config);
      }
      $SnSclient = new SnsClient($config);
      // 使用
      $topicArn = $config['arnprefix']."app/APNS_SANDBOX/IfulifeApp";
      if ($_GET['DEBUG']) {
        echo '<h4>目前AWS推播建立預設主題:$topicArn</h4>';
        var_dump($topicArn);
      }

      /**
       * @todo : 將手機註冊於aws sns IfulifeApp預設主題內取得aws端deviceId
       */
      // $subscribeParams = [
      //   'Protocol' => 'application',
      //   'ReturnSubscriptionArn' => true,
      //   'TopicArn' => $topicArn,
      // ];
      // $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      // $twNumber = $phoneUtil->parse($member->mobile, "TW");
      // $subscribeParams['Endpoint'] = $phoneUtil->format($twNumber, \libphonenumber\PhoneNumberFormat::E164);
      // $resp = $SnSclient->subscribe($subscribeParams);
      // if ($_GET['DEBUG']) {
      //   echo '<h4>請求AWS註冊aws_device_id回應結果:$resp[SubscriptionArn]</h4>';
      //   var_dump($resp);
      // }

      /**
       * NOTICE :
       * 1. 啟用帳號配置密碼後將紀錄門號資訊於AWS端以利後續取得推播訊息
       * 2. 產生更新時間並寫入資料庫
       */
      $member->password = $this->security->hash($dcpwd);
      $member->aws_device_id = $resp['SubscriptionArn'];
      $member->updated_at = date('Y-m-d H:i:s');
      if( !$member->save() ){
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $datas = $this->_setMemberExtend($member);
      if ($_GET['DEBUG']) {
        echo '<h4>回傳住戶資訊$datas:</h4>';
        var_dump( $datas);
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (AwsException $e) {
      echo $e->getAwsRequestId() . "<BR/>";
      echo $e->getAwsErrorType() . "<BR/>";
      echo $e->getAwsErrorCode() . "<BR/>";
      die();
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * 回傳主要資訊以便讓用戶端直接登入！
   * 沒有最後登入時間紀錄
   * 1. 所屬社區：戶別、棟號
   * 2. 物件所有權人資訊
   */
  protected function _setMemberExtend(&$member)
  {
    return [
      'owner' => $member->BrandenIfullCmtHousehold->owner,
      // from household
      'cmt' => $member->BrandenIfullCmtHousehold->BrandenIfullCmt->community,
      'cmtId' => $member->BrandenIfullCmtHousehold->cmt_id,
      'unit' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->unit,
      'unitId' => $member->BrandenIfullCmtHousehold->cmt_unit_id,
      // from unitId
      'block' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->BrandenIfullCmtBlock->block,
      'blockId' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->cmt_block_id,
      'activation_code' => $member->activation_code,
      // 是否開通
      'is_active' => $member->is_active,
      // 是否設定私人領件
      'is_private' => $member->is_private,
      // 是否同意隱私權政策
      'is_agree' => $member->is_agree,
      'fcm_token' => $member->fcm_token,
    ];
  }


  /**
   * post 處理登入
   * @param string $account
   * @param string $password(base64)
   *
   * @return json datas
   * */
  public function login()
  {
    $this->response->setContentType('application/json');

    try {
      $account = $this->request->getPost('account', 'string');
      $dcpwd = base64_decode($this->request->getPost('password', 'string'));
      if( !$account || !$dcpwd) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      //從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirstByMobile($account);
      if ($_GET['DEBUG']) {
        echo '<h4>取得的住戶資料$member:</h4>';
        var_dump( $member->toArray());
      }

      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號(手機門號)，請確認輸入資料正確。');
        throw $exc;
      }elseif( !$this->security->checkHash( $dcpwd, $member->password) ){
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_PASSWORD_FAIL;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('密碼錯誤，請更正後再次操作。');
        throw $exc;
      }

      $member->updated_at = date('Y-m-d H:i:s');
      if( !$member->save() ) {
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        $exc = new \Personalwork\Exceptions\Exception($sCode);
        $exc->setMessage('更新住戶登入時間發生錯誤: '.implode(',', $member->getMessages()));
        throw $exc;
      }

      $datas = $this->_setMemberExtend($member);
      $datas['id']=$member->id;
      $datas['name'] = $member->name;
      $datas['updated_at'] = $member->updated_at;
      if ($_GET['DEBUG']) {
        echo '<h4>登入成功資訊:</h4>';
        var_dump($datas);
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * post 修改密碼
   * @param string $memberId
   * @param string $oldpassword(base64)
   * @param string $newpassword(base64)
   *
   * @return json datas
   * */
  public function editpassword()
  {
    $this->response->setContentType('application/json');

    try {
      $memberId = $this->request->getPost('memberId', 'string');
      $dcoldpwd = base64_decode($this->request->getPost('oldpassword', 'string'));
      $dcnewpwd = base64_decode($this->request->getPost('newpassword', 'string'));
      if (!$memberId || !$dcoldpwd || !$dcnewpwd) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      //從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);
      if ($_GET['DEBUG']) {
        echo '<h4>取得的住戶資料$member:</h4>';
        var_dump($member->toArray());
      }

      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號，請確認您已登入。');
        throw $exc;
      } elseif (!$this->security->checkHash($dcoldpwd, $member->password)) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_PASSWORD_FAIL;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('舊密碼錯誤，請更正後再次操作。');
        throw $exc;
      }else{
        $member->password = $this->security->hash($dcnewpwd);
        if (!$member->save()) {
          $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
          $exc = new \Personalwork\Exceptions\Exception($sCode);
          $exc->setMessage('更改住戶登入密碼發生錯誤: ' . implode(',', $member->getMessages()));
          throw $exc;
        }

        $datas = [
          'code' => 200,
          'codeType' => 'OK',
          'msg' => '已完成更改您的登入密碼，下次請使用新密碼進入愛富生活'
        ];
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * get 處理登出
   * @todo : 後端紀錄登出狀態
   * */
  public function logout($id)
  {
    $this->response->setContentType('application/json');

    try {
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($id);
      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號(手機門號)，請確認輸入資料正確。');
        throw $exc;
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * post 處理建立會員裝置資訊用來推播紀錄
   * */
  public function fcm()
  {
    $this->response->setContentType('application/json');

    try {
      $memberId = $this->request->getPost('memberId', 'int');
      $deviceinfo = $this->request->getPost('deviceinfo', 'string');
      $fcm_token = $this->request->getPost('fcmToken', 'string');
      if (!$memberId || !$deviceinfo || !$fcm_token) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);
      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號，請確認輸入資料正確。');
        throw $exc;
      }

      $fcm=\Ifulifeapi\Models\BrandenIfullCmtHouseholdMemberDevices::findFirstByFcm_token($fcm_token);
      if( !$fcm || $fcm->cmt_household_member_id != $memberId){
        $fcm_record = new \Ifulifeapi\Models\BrandenIfullCmtHouseholdMemberDevices();
        $fcm_record->cmt_household_member_id = $memberId;
        $fcm_record->deviceinfo = $deviceinfo;
        $fcm_record->fcm_token = $fcm_token;
        $fcm_record->is_enable = 1;
        $fcm_record->noticetypes = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMemberDevices::NTYPE_DEFAULT;
        if( !$fcm_record->save() ){
          $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
          $exc = new \Personalwork\Exceptions\Exception($sCode);
          $exc->setMessage('建立住戶裝置識別碼發生錯誤: ' . implode(',', $fcm_record->getMessages()));
          throw $exc;
        }else{
          $member->fcm_token = $fcm_token;
          $member->save();
        }

        $datas = [
          'code' => 200,
          'codeType' => 'OK',
          'msg' => '已完成建立FCM資訊'
        ];
      }else{
        $datas = [
          'code' => 200,
          'codeType' => 'OK|NOACTION',
          'msg' => '重複的FCM資訊'
        ];
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * 設定私人包裹
   *
   * @return void
   */
  public function setPrivate() {
    $this->response->setContentType('application/json');

    try {
      $memberId = $this->request->getPost('memberId', 'int');
      $isPrivate = $this->request->getPost('isPrivate', 'int');
      if (!$memberId || !isset($isPrivate)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);
      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號，請確認輸入資料正確。');
        throw $exc;
      }

      $member->is_private = $isPrivate;
      if (!$member->save()) {
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        $exc = new \Personalwork\Exceptions\Exception($sCode);
        $exc->setMessage('設定住戶是否開啟私人領件發生錯誤: ' . implode(',', $member->getMessages()));
        throw $exc;
      }

      $datas = [
        'code' => 200,
        'codeType' => 'OK',
      ];
      if( $isPrivate ){
        $datas['msg'] = '已設定私人領件，請攜帶手機領取私人物品。';
      }else{
        $datas['msg'] = '已關閉私人領件功能。';
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * 設定同意條款
   *
   * @return void
   */
  public function setAgree(){
    $this->response->setContentType('application/json');

    try {
      $memberId = $this->request->getPost('memberId', 'int');
      $isAgree = $this->request->getPost('isAgree', 'int');
      if (!$memberId || !isset($isAgree)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if ($_GET['DEBUG']) {
        ob_start();
      }

      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);
      if (!$member) {
        $sCode = \Personalwork\Exceptions\AuthExceptions::AUTHEN_UNAUTHORIZED_NOTFOUND;
        $exc = new \Personalwork\Exceptions\AuthExceptions($sCode);
        $exc->setMessage('找不到帳號，請確認輸入資料正確。');
        throw $exc;
      }

      $member->is_agree = $isAgree;
      if( !$member->save() ) {
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        $exc = new \Personalwork\Exceptions\Exception($sCode);
        $exc->setMessage('設定住戶同意隱私權政策與服務條款發生錯誤: ' . implode(',', $member->getMessages()));
        throw $exc;
      }

      $datas = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已設定同意隱私權政策與服務條款'
      ];

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }
}