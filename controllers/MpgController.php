<?php
/**
 * 社區公告機制
 * 1. 取得所有社區公告
 * 2. 取得單一公告格式化內容欄位
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class MpgController extends Controller
{
  /**
   * GET 取得包裹到了列表
   * @todo : 加入jwt驗證
   * @todo : 加入篩選
   * @param int $memberId
   * @param string $state(未領/已領)
   *
   * @return json
   * */
  public function list($unitId, $memberId)
  {
    $this->response->setContentType('application/json');
    try {
      if (!intval( $unitId) || !intval( $memberId)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      //notice : 先取出戶別下所有包裹(根據最新到件時間排序)
      $mpgs = \Ifulifeapi\Models\BrandenIfullMpg::findByCmt_unit_id($unitId, ['order' => "receipt_at DESC"]);
      if (!count($mpgs)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $notyet = $took = $rejects = [];
      foreach ($mpgs as $mpg) {
        $data = [
          // 包裹編號
          'id' => $mpg->id,
          // 所屬帳號
          'onwerMemberId' => $mpg->cmt_household_member_id,
          'onwerMemberName' => $mpg->OnwerMember->name,
          // 類型
          'mpgTypeLabel' => $mpg->BrandenIfullMpgType->mpg_type,
          // 編號
          'mpgcode' => $mpg->mpgcode,
          // 條碼
          'barcode' => $mpg->barcode,
          // 包裹／掛號進件時間
          'receipt_at' => date('Y/m/d H:i', strtotime($mpg->receipt_at)),
          // 備註
          'remark' => $mpg->remark
        ];
        // 物流廠商
        if ($mpg->logistics_id) {
          $logistics = \Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findFirst([
            "cmn_define_id=18 AND id={$mpg->logistics_id}"
          ]);
          // 物流編號
          $data['logistics_id'] = $mpg->logistics_id;
          $data['logisticsLabel']=$logistics->noun;
        }

        if (!empty($mpg->return_apply_at)) {
          $data['state'] = 2;
          $data['stateLabel'] = '申請退件';
          $data['returnMemberName'] = $mpg->ReturnMember->name;
          $data['return_apply_at'] = date('Y/m/d H:i', strtotime($mpg->return_apply_at));
          //notice : 判斷是否完成退件申請狀態用。
          $data['return_code'] = $mpg->return_code;
          $rejects[] = $data;
        } elseif (!empty($mpg->collar_at)) {
          $data['state'] = 1;
          $data['stateLabel'] = '已領件';
          $data['collar_at'] = date('Y/m/d H:i', strtotime($mpg->collar_at));
          $data['rcpMemberId'] = $mpg->rcp_cmt_household_member_id;
          $data['rcpMemberName'] = $mpg->RcpMember->name;
          $took[] = $data;
        } else {
          $data['state'] = 0;
          $data['stateLabel'] = '未領件';
          $notyet[] = $data;
        }
      }
      $datas = [
        'notyet' => $notyet,
        'took' => $took,
        'rejects' => $rejects
      ];

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html;charset=UTF-8;');
        ob_start();
        echo '<h4>取得的住戶包裹資訊$mpgs:</h4>';
        var_dump($mpgs->toArray());
        $html .= ob_get_contents(void);
        echo '<h4>轉換回傳$mpgs:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      if($datas['codeType']== "APPLICATION_ROWDATA_NOTFOUND"){
        $this->response->setStatusCode(200,'NODATA');
        $datas=[];
      }else{
        $this->response->setStatusCode($err->getCode(), $datas['codeType']);
      }
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * 處理退件機制
   * @param int $mpgId
   * @param int $memberId
   *
   * @return void
   */
  public function reject()
  {
    $this->response->setContentType('application/json');
    try {
      $mpgId = $this->request->getPost('mpgId', 'string');
      $memberId = $this->request->getPost('memberId', 'string');

      if (!intval($mpgId) || !intval($memberId)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $mpg = \Ifulifeapi\Models\BrandenIfullMpg::findFirst($mpgId);
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst( $memberId);
      //notice : 目前例外僅針對程式除錯設計，未詳細考慮用戶端看的訊息。
      if (!$mpg || !$member) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html');
        ob_start();
        echo '<h4>指定退件$mpg:</h4>';
        var_dump($mpg->toArray());
      }

      //notice : 目前並未處理檢查退件規則，僅僅配置退件時間
      $mpg->return_apply_at = date('Y-m-d H:i:s');
      //notice : 改變規則，由於App端必須針對退貨列表顯示獨立的稽核紀錄，必須改成直接紀錄狀態時間點。
      $mpg->return_cmt_household_member_id = $memberId;
      // if( $mpg->cmt_household_member_id != $memberId ){
      //   $mpg->remark = "由{$member->name}申請退貨。";
      // }

      if( !$mpg->save() ){
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        $exc = new \Personalwork\Exceptions\Exception($sCode);
        $exc->setMessage('寫入包裹/掛號退件紀錄發生錯誤: ' . implode(',', $mpg->getMessages()));
        throw $exc;
      }

      $datas = [
        'code' => 200,
        'msg'  => "本件{$mpg->BrandenIfullMpgType->mpg_type}已申請退件，請記得交付物品給予櫃檯。"
      ];

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }
}