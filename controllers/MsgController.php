<?php

/**
 * 最新消息(社區公告)機制
 * 1. 取得所有最新消息(社區公告)
 * 2. 取得單一公告格式化內容欄位
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class MsgController extends Controller
{
  /**
   * GET 取得最新消息(社區公告)列表
   * @todo : 加入jwt驗證
   * @param string 公告類型
   *
   * @return json
   * */
  public function newest($unitId)
  {
    $this->response->setContentType('application/json');
    try{
      // 根據戶別找社區
      $unit = \Ifulifeapi\Models\BrandenIfullCmtUnit::findFirst($unitId);
      if (!$unit) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $params = [
        "cmt_id=:cmt: AND is_ontop=1 AND is_active=1",
        "bind" => [
          'cmt' => $unit->cmt_id,
        ],
        "order" => ["updated_at DESC"]
      ];
      $newest = \Ifulifeapi\Models\BrandenIfullMsg::find($params)
                //notice : 排除只針對特定住戶的訊息！
                ->filter(function($row) use ($unitId){
                  if( count($row->getBrandenIfullMsgUnit()) &&
                      $row->getBrandenIfullMsgUnit(["cmt_unit_id!=$unitId"])
                  ) {
                    return false;
                  }
                  return \Ifulifeapi\Models\BrandenIfullMsg::findFirst($row->id);
                });
      if (!$newest) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $datas=[];
      foreach ($newest as $msg) {
        $data = [
          'id' => $msg->id,
          'title' => $msg->title,
          // 最新消息(社區公告)類型定義
          'msgType' => $msg->BrandenIfullCmnMsgType->msg_type,
          'msgTypeSname' => $msg->BrandenIfullCmnMsgType->msg_type_sname,
          'createStamp' => strtotime($msg->created_at),
          'createFormat' => date('Y/m/d', strtotime($msg->created_at)),
        ];

        if (isset($_GET['DEBUG'])) {
          echo '<h4>單則訊息內容$msg:</h4>';
          var_dump($data);
          continue;
        }
        $datas[] = $data;
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      $this->response->setContent(json_encode($datas));
      return $this->response->send();
    }
  }


  /**
   * POST 設定
   * @todo : 加入jwt驗證
   * @param string 公告類型
   *
   * @return json
   * */
  public function setreaded()
  {
    $this->response->setContentType('application/json');
    // $this->response->setContentType('text/html');

    try {
      // 取得並判斷必要變數
      $msgId = $this->request->getPost('msgId', 'int');
      $memberId = $this->request->getPost('memberId', 'int');

      if( empty($msgId) || empty($msgId) ){
        $sCode =\Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception( $sCode );
      }

      //notice : 找到對應紀錄並檢查是否已經設定過，設定過則忽略處理回傳
      $msg = \Ifulifeapi\Models\BrandenIfullMsg::findFirst($msgId);
      if( !$msg ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }
      $param=[
        "msg_id=:msg: AND cmt_household_member_id=:member:",
        'bind' => [
          'msg' => $msgId,
          'member' => $memberId,
        ]
      ];
      $msgread = \Ifulifeapi\Models\BrandenIfullMsgRead::findFirst($param);
      // 已設定已讀
      if( $msgread ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_PROCESSED;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }else{
        $readed = new \Ifulifeapi\Models\BrandenIfullMsgRead();
        $readed->msg_id = $msg->id;
        $readed->cmt_household_member_id = $memberId;
        $readed->created_at = date('Y-m-d H:i:s');
        $readed->updated_at = date('Y-m-d H:i:s');
        if( !($id = $readed->save()) ){
          $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
          $exc= new \Personalwork\Exceptions\Exception($sCode);
          $exc->setMessage('寫入已讀公告紀錄發生錯誤: '.implode(',',$readed->getMessages()));
          throw $exc;
        }
      }

      $this->response->setStatusCode(200, 'OK');
      $datas = [
        'code' => 200,
        'id' => $readed->id,
        'msg' => '已將該筆公告設定已讀'
      ];
    } catch(\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode( $err->getCode(), $datas['codeType'] );
    } finally {
      $this->response->setContent(json_encode( $datas));
      return $this->response->send();
    }
  }

  /**
   * GET 取得最新消息(社區公告)列表
   * @todo : 加入jwt驗證
   * @todo : 加入$filter_type篩選
   * @param string 公告類型
   *
   * @return json
   * */
  public function list($unitId, $memberId, $filter_type)
  {
    $this->response->setContentType('application/json');
    try {
      if( !intval($unitId) || !intval($memberId) ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      // 根據戶別找社區
      $unit =\Ifulifeapi\Models\BrandenIfullCmtUnit::findFirst($unitId);
      if( !$unit ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }
      // 根據戶別取出棟別編號
      $blockId=$unit->cmt_block_id;

      // 預設規則:先抓出啟用的訊息
      $params =[
        "cmt_id=:cmt: AND is_active=1",
        "bind" => [
          'cmt' => $unit->cmt_id,
        ],
        "order" => ["is_ontop DESC", "updated_at DESC"]
      ];
      // if( $filter_type == ''){}
      $msgs = \Ifulifeapi\Models\BrandenIfullMsg::find($params);
      $datas = [];
      // 列表頁籤未讀數量
      $unread = ['news' => 0, 'events' => 0, 'units' => 0,];
      // 社區公告
      $news = [];
      // 活動通知
      $events = [];
      // 住戶私訊
      $units = [];

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html');
        ob_start();
      }

      foreach ($msgs as $msg) {
        //notice : 當設定訊息設定棟別且用戶不屬於該棟別排除訊息
        // if( count($msg->BrandenIfullMsgBlock) &&
        //     !count( $msg->getBrandenIfullMsgBlock(["cmt_block_id=$blockId"])) ){
        //   continue;
        // }

        $data = [
          'id' => $msg->id,
          'title' => $msg->title,
          // 社區公告類型定義
          'msgType' => $msg->BrandenIfullCmnMsgType->msg_type,
          'msgTypeSname' => $msg->BrandenIfullCmnMsgType->msg_type_sname,

          'create_at' => $msg->created_at,
          'createStamp' => strtotime($msg->created_at),
          'createFormat' => date('Y/m/d', strtotime($msg->created_at)),
          // 判斷值是否(啟用)顯示
          'is_active' => $msg->is_active,
          // 判斷值是否置頂
          'is_ontop' => $msg->is_ontop,
        ];
        //notice : 列表改成最新消息，使用顏色區分已讀與未讀
        if( count($msg->getBrandenIfullMsgRead()) &&
            $msg->getBrandenIfullMsgRead(["cmt_household_member_id=$memberId"])->getFirst() ){
          $data['readed'] = 1;
        }else{
          $data['readed'] = 0;
        }

        //notice : 判斷針對住戶資訊(不再顯示於社區公告與活動通知)
        if( count($msg->getBrandenIfullMsgUnit()) &&
        $msg->getBrandenIfullMsgUnit(["cmt_unit_id=$unitId"])
        ) {
          $units[] = $data;
          if( $data['readed'] == 0 ){
            $unread['units']++;
          }
        //notice : 判斷是否為活動通知列表
        }else if( $msg->is_event ){
          $events[] = $data;
          if ($data['readed'] == 0) {
            $unread['events']++;
          }
        }else{
          $news[] = $data;
          if ($data['readed'] == 0) {
            $unread['news']++;
          }
        }

        if (isset($_GET['DEBUG'])) {
          echo '<h4>單則訊息內容$msg:</h4>';
          var_dump($data);
          continue;
        }
      }

      if( count($news) ){
        $datas['news'] = $news;
      }
      if (count($events)) {
        $datas['events'] = $events;
      }
      if (count($units)) {
        $datas['units'] = $units;
      }
      //notice : 顯示於頁籤標題未讀數字
      $datas['unread']=$unread;

      if (isset($_GET['DEBUG'])) {
        echo '<h4>訊息列表回傳結構$msgs:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      }else{
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * GET 取得單一公告格式化內容欄位
   *
   * @return json
   * */
  public function detail($id)
  {
    $this->response->setContentType('application/json');
    try {
      if (!$msgId = intval($id)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $msg = \Ifulifeapi\Models\BrandenIfullMsg::findFirst($msgId);
      if (!$msg) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html');
        ob_start();
        echo '<h4>訊息$msg</h4>';
        var_dump($msg->toArray());
      }

      // 判斷附檔、附檔顯示類型：json / pdf / image
      $files = \Ifulifeapi\Models\SystemFiles::find([
        'attachmentType' => get_class($msg),
        'attachmentId' => $msg->id
      ]);
      if (isset($_GET['DEBUG'])) {
        echo '<h4>訊息是否有附檔$files</h4>';
        var_dump($files);
      }

      //notice : 修正連結配置問題''不回傳！
      if( $msg->url == ''){
        $msg->url = null;
      }
      $datas = [
        'id' => $msg->id,
        'title' => $msg->title,
        'url' => $msg->url,
        'msgType' => $msg->BrandenIfullCmnMsgType->msg_type,
        'msgTypeSname' => $msg->BrandenIfullCmnMsgType->msg_type_sname,
        'message' => $msg->message,
        'start_at' => $msg->start_at,
        'startFormat' => date('Y/m/d H:i', strtotime($msg->start_at)),
        'startUTC' => date('Y-m-d\TH:i:s.000\Z', strtotime($msg->end_at)),
        'end_at' => $msg->end_at,
        'endFormat' => date('Y/m/d H:i', strtotime($msg->end_at)),
        'endUTC' => date('Y-m-d\TH:i:s.000\Z', strtotime($msg->end_at)),
        'attachments' => $files
      ];
      if (isset($_GET['DEBUG'])) {
        echo '<h4>詳細訊息回傳結構$msgs:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  protected function _getMsgType() {
    $msg_type_mapping = [
      1 => [
        'label' => '系統公告',
        'slabel' => '系統',
        'color' => '#c64a3d',
      ],
      2 => [
        'label' => '社區公告',
        'slabel' => '社區',
        'color' => '#4992f9',
      ],
      3 => [
        'label' => '活動通知',
        'slabel' => '活動',
        'color' => '#5fba7d',
      ],
      4 => [
        'label' => '瓦斯抄表',
        'slabel' => '抄表',
        'color' => '#c469d0',
      ],
    ];
    return $msg_type_mapping;
  }


  /**
   * GET / 取得行事曆活動對應社區公告資訊：標題、時間、部分文字
   * @param int 社區編號
   * @param string 開始日期到結束日期
   *
   * @return json
   * */
  public function calendar($cmtId, $datestart, $dateend)
  {
    $this->response->setContentType('application/json');
    try {
      //從msg內找出社區下所有事件並過濾在時間範圍內
      if( !$cmtId ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $params = [
        "cmt_id=:cmt:
        AND is_event=1
        AND is_active=1
        AND start_at >= :start:
        AND end_at <= :end:",
        "bind" => [
          "cmt" => $cmtId,
          "start" => $datestart,
          "end" => $dateend,
        ],
        "columns" => [
          "id",
          "passdays" => "DATEDIFF(end_at, start_at)",
          "title",
          "message",
          "start_at",
          "end_at",
          "cmn_msg_type_id"
        ],
        "order" => "start_at ASC"
      ];
      $events = \Ifulifeapi\Models\BrandenIfullMsg::find($params);
      if (!count($events)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html');
        ob_start();
        echo '<h4>行事曆活動$events</h4>';
        var_dump($events->toArray());
      }

      $datas = [];
      $eventTypeMapping = $this->_getMsgType();
      // 配置事件類型對應顏色
      foreach( $events as $event ){
        // $data = [
        // 'msgId'        => $event->id,
        // 'msgTitle'     => $event->title,
        // 'msgTypeId'   => $event->cmn_msg_type_id,
        // 'eventRange' => date('Y/m/d', strtotime($event->start_at)).'~'. date('Y/m/d', strtotime($event->end_at))
        // 'message'   => $event->message,
        // 'start_at'  => $event->start_at,
        // 'startFormat' => date('Y/m/d', strtotime($event->start_at)),
        // 'end_at'    => $event->end_at,
        // 'endFormat' => date('Y/m/d', strtotime($event->end_at)),
        // ];
        // 計算日期範圍天數跑回圈
        $sDate = new \DateTime($event->start_at);
        for( $i=0; $i<=$event->passdays ; $i++ ){
          $date_key=$sDate->add(new \DateInterval("P{$i}D"))->format('Y-m-d');
          $dotMark =[
            'key' => uniqid(),
            'color' => $eventTypeMapping[$event->cmn_msg_type_id]['color'],
            'typeId' => $event->cmn_msg_type_id,
            'typeSlabel' => $eventTypeMapping[$event->cmn_msg_type_id]['slabel']
          ];
          if( !array_key_exists($date_key, $datas) ){
            $datas[$date_key]= [
              'marked' => true,
              'dots' => [
                0=> $dotMark
              ]
            ];
          }else{
            $res=array_search( $event->cmn_msg_type_id, array_column( $datas[$date_key]['dots'], 'typeId'));
            if( !$res ){
              array_push( $datas[$date_key]['dots'], $dotMark);
            }
          }
        }
      }

      if (isset($_GET['DEBUG'])) {
        echo '<h4>詳細行事曆回傳結構$datas:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setContent( $err->getMessage() );
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * GET / 取得行事曆列表模式活動對應社區公告資訊：標題活動、時間範圍、時間長度
   * @param int 社區編號
   * @param string 開始日期
   *
   * @return json
   * */
  public function calendarlist($cmtId, $datestart)
  {
    $this->response->setContentType('application/json');
    try{
      if (!$cmtId) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      $eventTypeMapping = $this->_getMsgType();

      $params = [
        "cmt_id=:cmt:
        AND is_event=1
        AND is_active=1
        AND end_at >= :setdate:",
        "columns" => [
          "id",
          "title",
          "start_at",
          "end_at",
          "cmn_msg_type_id"
        ],
        "order" => "start_at ASC"
      ];
      $time = strtotime($datestart);

      if (isset($_GET['DEBUG'])) {
        $this->response->setContentType('text/html');
        ob_start();
      }

      //notice : 查詢規則，日期在(社區公告)訊息範圍內且為指定社區的事件
      $datas = [];
      for($i=0; $i<7; $i++){
        $setdate = date('Y-m-d', $time + 86400 * $i);
        $params["bind"] = [
          "cmt" => $cmtId,
          "setdate" => $setdate,
        ];
        $events = \Ifulifeapi\Models\BrandenIfullMsg::find($params);
        if( count( $events) ){
          $eventrows=[];
          foreach( $events as $event){
            //notice 判斷是否設定開始時
            $d=explode('-',$setdate);
            if( date('d', strtotime( $event->end_at)) != $d[2]){
              $duration = '整日活動';
            }else{
              $duration = '於'.date('H', strtotime($event->end_at)).'時結束';
            }

            $eventrows[] = [
              'id'          => $event->id,
              'title'       => $event->title,
              'typelabel'  => $eventTypeMapping[$event->cmn_msg_type_id]['label'],
              'typeSlabel'  => $eventTypeMapping[$event->cmn_msg_type_id]['slabel'],
              'duration'    => $duration,
              'start_at'    => $event->start_at,
              'end_at'      => $event->end_at,
              'rangeString' => "{$event->start_at}~{$event->end_at}",
            ];
          }
          $datas[] = [
            'title' => $setdate,
            'data' => $eventrows
          ];
        }else{
          $datas[]=[
            'title' => $setdate,
            'data' => []
          ];
        }
      }

      if (isset($_GET['DEBUG'])) {
        echo '<h4>行事曆列表依照日期回傳活動結構$datas:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setContent($err->getMessage());
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }
}