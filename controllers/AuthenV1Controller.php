<?php
/**
 * 處理住戶開通認證機制
 * 1. 人工由於物管人員登入基本用戶資料建立 BrandenIfullCmtHouseholdMember
 * 2. 從App介面（首頁=>我的=>判斷若未開通[確認帳號開始使用]=>未登入[啟用裝置]）
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;
use Phalcon\Crypt;
class AuthenV1Controller extends Controller
{

  private function _generateRandomString($length = 6) {
    // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  protected function _setCode($code)
  {
    switch ($code) {
      case 1:
        return [
          'code' => 500,
          'codeType' => "APPLICATION_ERROR"
        ];
        break;
      case 5401:
        return [
          'code' => 403,
          'codeType' => "AUTHEN_NOTFOUND"
        ];
        break;
      case 5402:
        return [
          'code' => 403,
          'codeType' => "AUTHEN_FAIL"
        ];
        break;
      default:
        return [
          'code' => 500,
          'codeType' => "APPLICATION_ERROR"
        ];
        break;
    }
  }


  /**
   * 回傳主要資訊以便讓用戶端直接登入！
   * 沒有最後登入時間紀錄
   * 1. 所屬社區：戶別、棟號
   * 2. 物件所有權人資訊
   */
  protected function _setMemberExtend(&$member)
  {
    return [
      'owner' => $member->BrandenIfullCmtHousehold->owner,
      // from household
      'cmt' => $member->BrandenIfullCmtHousehold->BrandenIfullCmt->community,
      'cmtId' => $member->BrandenIfullCmtHousehold->cmt_id,
      'unit' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->unit,
      'unitId' => $member->BrandenIfullCmtHousehold->cmt_unit_id,
      // from unitId
      'block' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->BrandenIfullCmtBlock->block,
      'blockId' => $member->BrandenIfullCmtHousehold->BrandenIfullCmtUnit->cmt_block_id,
      'aws_device_id' => $member->aws_device_id,
    ];
  }

  /**
   * post
   * 處理開通
   * 1. 對開通主題(ifulife_deviceactive)，註冊訂閱者然後發送訊息後馬上刪除訂閱
   * 2. 將產生的訂閱紀錄加到預設主題(ifulife)，確保註冊訂閱者產生的id一致
   *
   * @return code（加密處理，App端可以對應解密！）
   * */
  public function identity($phonenumber)
  {
    $response = new Response();
    $response->setContentType('application/json');

    try {
      // act1. 從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirstByMobile($phonenumber);
      if (!$member) {
        $codeInfo = $this->_setCode(5401);
        $codeInfo['errMsg'] = '找不到使用者帳號，請確認輸入資料正確。';
        $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
        if ($_GET['DEBUG']) {
          $response->setContentType('text/html');
          var_dump($data);
        } else {
          $response->setContent(json_encode($data));
        }
        return $response;
      }

      // 代表已經開通過
      if ( $member->aws_device_id) {
        $response->setStatusCode(200, "OK");
        if( !$member->password ){
          $data = [
            'code' => '200',
            'codeType' => 'HAVE_ACTIVED',
            'msg' => '您已經完成開通'
          ];
        }else{
          $data = [
            'code' => '200',
            'codeType' => 'FINISH_ACTIVED',
            'msg' => '您已經完成開通與註冊，請使用密碼直接登入使用。'
          ];
        }
        if ($_GET['DEBUG']) {
          $response->setContentType('text/html');
          echo "<h4>認證狀態: {$data['codeType']}</h4>";
          var_dump($data);
        } else {
          $response->setContent(json_encode($data));
        }
        return $response;
      }

      // 產生開通碼、更新時間並寫入資料庫
      if( !$member->activation_code ){
        /**
         * @todo 測試期間開通碼固定使用123456
         */
        // $act_code = $this->_generateRandomString(8);
        $act_code = '123456';
        $member->activation_code= $act_code;
      }else{
        $act_code = $member->activation_code;
      }
      $member->updated_at = date('Y-m-d H:i:s');
      $member->save();

      $config = $this->config->aws->toArray();
      $SnSclient = new SnsClient($config);

      $Message = '您的愛富平台App開通碼: '.$act_code;
      $phoneUtil = PhoneNumberUtil::getInstance();
      $twNumber = $phoneUtil->parse($member->mobile, "TW");
      $PhoneNumber = $phoneUtil->format($twNumber, \libphonenumber\PhoneNumberFormat::E164);

      /**
       * 2019-06-14
       * aws簡訊機制作法暫停！
       */
      $sendData =[
        'Message' => $Message,
        'PhoneNumber' => $PhoneNumber,
      ];
      // $result = $SnSclient->publish($sendData);

      $respData = [
        'id' => $member->id,
        'phone' => $member->mobile,
        'name' => $member->name,
        'code' => $member->activation_code,
        'updated_at' => $member->updated_at,
      ];

      $response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $response->setContentType('text/html');
        echo '<h4>awssns服務回應:</h4>';
        var_dump( $result);
        echo '<h4>回傳開通資訊:</h4>';
        var_dump( $data);
      } else {
        $response->setContent(json_encode($respData));
      }

    } catch (\Exception $e) {
      if ($e->getCode()) {
        $codeInfo = $this->_setCode($e->getCode());
        $codeInfo['errMsg'] = $e->getMessage();
        $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      }
      $res = json_encode($codeInfo);
      $response->setContent($res);
    }
    return $response;
  }


  /**
   * post
   * 處理開通後註冊裝置與設定密碼機制
   * 1. 將密碼以及awsID資訊加入資料庫紀錄
   * 2. 處理取得完整住戶資訊回傳App
   *
   * @param string $account
   * @param string $actcode
   *
   * @return mixed result
   * */
  public function register()
  {
    $response = new Response();
    $response->setContentType('application/json');

    $memberId = $this->request->getPost('id', 'int');
    $dcpwd=base64_decode($this->request->getPost('password', 'string'));
    try {
      // act1. 從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);

      if (!$member) {
        $codeInfo = $this->_setCode(5401);
        $codeInfo['errMsg'] = '找不到使用者帳號，請確認輸入資料正確。';
        $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
        if ($_GET['DEBUG']) {
          $response->setContentType('text/html');
          var_dump($data);
        } else {
          $response->setContent(json_encode($data));
        }
        return $response;
      }

      /**
       * @todo : 確認如何實作忘記密碼流程？
       * 依此作法是重新設定註冊密碼，可以判斷略過aws配置deviceId那段
       */
      // if ($member->password) {
      //   $data = [
      //     'code' => '200',
      //     'codeType' => 'FINISH_ACTIVED',
      //     'msg' => '您已經完成開通與註冊，請使用密碼直接登入使用。'
      //   ];
      // }
      // if ($_GET['DEBUG']) {
      //   $response->setContentType('text/html');
      //   echo "<h4>認證狀態: {$data['codeType']}</h4>";
      //   var_dump($data);
      // } else {
      //   $response->setContent(json_encode($data));
      //   return $response;
      // }

      $config = $this->config->aws->toArray();
      $SnSclient = new SnsClient($config);
      // 使用預設主題
      $topicArn = $config['arnprefix']."ifulife";

      // 將手機註冊於aws sns Ifulife預設主題內取得aws端deviceId
      $subscribeParams = [
        'Protocol' => 'sms',
        'ReturnSubscriptionArn' => true,
        'TopicArn' => $topicArn,
      ];
      $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
      $twNumber = $phoneUtil->parse($member->mobile, "TW");
      $subscribeParams['Endpoint'] = $phoneUtil->format($twNumber, \libphonenumber\PhoneNumberFormat::E164);
      $resp = $SnSclient->subscribe($subscribeParams);


      /**
       * @todo : 討論關於狀態配置作法！
       * 1. 單次開通後啟用帳號，
       * 2. 目前由於只設定手機門號，故用戶認證只有單一認證方式簡訊，有被替換風險以及加強唯一識別機制。
       * 產生更新時間並寫入資料庫
       */
      $member->password = $this->security->hash($dcpwd);
      $member->aws_device_id = $resp['SubscriptionArn'];
      $member->updated_at = date('Y-m-d H:i:s');
      $member->save();

      $returndata = $this->_setMemberExtend($member);

      $response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $response->setContentType('text/html');
        echo '<h4>訂閱裝置到預設主題取得裝置識別碼:</h4>';
        var_dump($result);
        echo '<h4>回傳住戶資訊:</h4>';
        var_dump($returndata);
      } else {
        $response->setContent(json_encode($returndata));
      }
    } catch (\Exception $e) {
      $codeInfo = $this->_setCode($e->getCode());
      $codeInfo['errMsg'] = $e->getMessage();
      $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      $response->setContent(json_encode($codeInfo));
      error_log($e->getMessage());
    }
    return $response;
  }


  /**
   * post
   * 處理登入
   * */
  public function login()
  {
    $this->response->setContentType('application/json');

    try {
      $account = $this->request->getPost('account', 'string');
      $dcpwd = base64_decode($this->request->getPost('password', 'string'));

      // act1. 從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirstByMobile($account);
      if (!$member) {
        $codeInfo=$this->_setCode(5401);
        $codeInfo['errMsg'] = '找不到使用者帳號，請確認輸入資料正確。';
        $this->response->setStatusCode( $codeInfo['code'], $codeInfo['codeType']);
        if ($_GET['DEBUG']) {
          $this->response->setContentType('text/html');
          var_dump($data);
        } else {
          $this->response->setContent(json_encode($data));
        }
        return $this->response->send();
      }elseif( !$this->security->checkHash('111', $member->password) ){
        $codeInfo = $this->_setCode(5402);
        $codeInfo['errMsg'] = '密碼錯誤，請重新操作。';
        $this->response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
        if ($_GET['DEBUG']) {
          $this->response->setContentType('text/html');
          var_dump($data);
        } else {
          $this->response->setContent(json_encode($codeInfo));
        }
        return $this->response->send();
      }

      /**
       * @todo : 更新最後登入時間
       */
      $member->updated_at = date('Y-m-d H:i:s');
      $member->save();

      $returndata = $this->_setMemberExtend($member);
      $returndata['id']=$member->id;
      $returndata['name'] = $member->name;
      $returndata['updated_at'] = $member->updated_at;

      $this->response->setStatusCode(200, 'OK');
      if ($_GET['DEBUG']) {
        $this->response->setContentType('text/html');
        echo '<h4>登入傳遞帳號密碼(透過json轉換$_POST):</h4>';
        var_dump($_POST);
        echo '<h4>回傳住戶資訊:</h4>';
        var_dump($returndata);
      } else {
        $this->response->setContent(json_encode($returndata));
      }
    } catch (\Exception $e) {
      $codeInfo = $this->_setCode($e->getCode());
      $codeInfo['errMsg'] = $e->getMessage();
      $this->response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      $this->response->setContent(json_encode($codeInfo));
      error_log($e->getMessage());
    }
    return $this->response->send();
  }



  /**
   * put
   * 處理登出
   * 後端紀錄登出狀態
   * */
  public function logout($id)
  {
    $response = new Response();

    try {
      // act1. 從BrandenIfullCmtHouseholdMember比對 phone
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($id);
      if (!$member) {
        $codeInfo = $this->_setCode(5401);
        $codeInfo['errMsg'] = '找不到使用者帳號，請確認輸入資料正確。';
        $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
        if ($_GET['DEBUG']) {
          $response->setContentType('text/html');
          var_dump($data);
        } else {
          $response->setContent(json_encode($data));
        }
        return $response;
      }

    } catch (\Exception $e) {
      $codeInfo = $this->_setCode($e->getCode());
      $codeInfo['errMsg'] = $e->getMessage();
      $response->setStatusCode($codeInfo['code'], $codeInfo['codeType']);
      $response->setContent(json_encode($codeInfo));
      error_log($e->getMessage());
    }
    return $response;
  }
}