<?php
/**
 * 意見提出機制
 * 1. 意見提出表單
 * 2. 紀錄列表
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class OpinionController extends Controller
{
  /**
   * POST / 寫入意見提出資訊
   * @todo : 加入jwt驗證
   * @param string 公告類型
   *
   * @return json
   * */
  public function form()
  {
    $this->response->setContentType('application/json');
    // $this->response->setContentType('text/html');

    try {
      // 取得並判斷必要變數
      $memberId = $this->request->getPost('memberId', 'int');
      if( empty( $memberId) ){
        $sCode =\Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception( $sCode );
      }
      $member = \Ifulifeapi\Models\BrandenIfullCmtHouseholdMember::findFirst($memberId);

      $opinion = new \Ifulifeapi\Models\BrandenIfullOpinion();
      $opinion->cmt_id = $member->BrandenIfullCmtHousehold->cmt_id;
      //fixed : 意見提出必須加入戶別編號(未來改多戶時，要由App端送出資訊！)
      $opinion->cmt_unit_id = $member->BrandenIfullCmtHousehold->cmt_unit_id;
      $opinion->cmt_household_id = $member->BrandenIfullCmtHousehold->id;
      $opinion->cmt_household_member_id = $memberId;

      //notice : 案件編號取主鍵加1補滿10位
      $no = \Ifulifeapi\Models\BrandenIfullOpinion::maximum(['column' => 'id']);
      if (!$no) {
        $setno = date('Ym') . str_pad(1, 3, 0, STR_PAD_LEFT);
      } else {
        $setno = date('Ym') . str_pad($no + 1, 3, 0, STR_PAD_LEFT);
      }
      $opinion->opinion_no = $setno;
      $opinion->opinion_type_id = $this->request->getPost('selectedType', 'int');
      $opinion->phone_model = $this->request->getPost('appDeviceValue', 'string');
      $opinion->opinion_date = date('Y-m-d H:i:s');
      $opinion->opinion = $this->request->getPost('textarea', 'string');
      // 從branden_ifull_cmn_define_noun取出意見類別
      $opinnionStatus = \Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findByCmn_define_id(6)->getFirst();
      $opinion->opinion_status = $opinnionStatus->noun_code;
      $opinion->created_at = date('Y-m-d H:i:s');
      $opinion->updated_at = date('Y-m-d H:i:s');
      if (!$opinion->save()) {
        $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
        $exc = new \Personalwork\Exceptions\Exception($sCode);
        $exc->setMessage( '儲存意見提出發生錯誤: ' . implode(',', $opinion->getMessages()));
        throw $exc;
      }

      // 處理檔案上傳
      if (is_array($_FILES['photo'])) {
        if (!$_FILES['photo']['error']) {
          $_FILES['photo']['haseVal'] = $this->security->hash($_FILES['photo']['name']);
          $system = new \Ifulifeapi\Models\SystemFiles();
          if (!$system->uploadToOctoberPath($_FILES['photo'])) {
            $sCode = \Personalwork\Exceptions\Exception::FILE_UPLOAD_ERROR;
            $exc = new \Personalwork\Exceptions\Exception($sCode);
            $exc->setMessage('暫存檔案複製儲存錯誤(octobercms路徑)');
            throw $exc;
          }
          $system->description = 'Upload from App';
          $system->field = 'images';
          $system->attachment_id = $opinion->id;
          $system->attachment_type = \Ifulifeapi\Models\SystemFiles::getAttachmentTypeMapping('Ifulifeapi\Models\Opinion');
          $system->is_public = 1;
          // 找出最大sort_order累加
          $newOrder = intval(\Ifulifeapi\Models\SystemFiles::maximum(['column' => 'sort_order'])) + 1;
          $system->sort_order = $newOrder;
          $system->created_at = date('Y-m-d H:i:s');
          $system->updated_at = date('Y-m-d H:i:s');
          if (!$system->save()) {
            $sCode = \Personalwork\Exceptions\Exception::DATABASE_INSERT_ERROR;
            $exc = new \Personalwork\Exceptions\Exception($sCode);
            $exc->setMessage('相片紀錄資料庫發生錯誤：' . implode(',', $system->getMessages()));
            throw $exc;
          }
        } else {
          $sCode = \Personalwork\Exceptions\Exception::FILE_UPLOAD_ERROR;
          $exc = new \Personalwork\Exceptions\Exception($sCode);
          $exc->setMessage('相片上傳發生錯誤');
          throw $exc;
        }
      }

      $this->response->setStatusCode(200, 'OK');
      $datas = [
        'id' => $opinion->id,
        'opinionNo' => $opinion->opinion_no,
        'statusLabel' => $opinnionStatus->noun,
        'msg' => "您的意見已建檔，後續處理進度可到紀錄確認。"
      ];
    } catch(\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode( $err->getCode(), $datas['codeType'] );
    } finally {
      $this->response->setContent(json_encode( $datas));
      return $this->response->send();
    }
  }

  /**
   * GET 取得意見回覆紀錄列表
   * @todo : 加入jwt驗證
   * @param int 戶別編號
   * @param int 用戶編號
   *
   * @return json
   * */
  public function list($unitId, $memberId)
  {
    $this->response->setContentType('application/json');
    try {
      if( !intval($unitId) || !intval($memberId) ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      // 根據戶別找出意見清單
      $opinions =\Ifulifeapi\Models\BrandenIfullOpinion::findByCmt_unit_id($unitId);
      if( !$opinions ){
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }
      //意見類型
      $opinionTypes = \Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findByCmn_define_id(10);
      $typeArr=[];
      foreach ($opinionTypes as $type) {
        $typeArr[$type->noun_code] = $type->toArray();
      }
      //意見狀態
      $opinionStatuses = \Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findByCmn_define_id(6);
      $statusArr = [];
      foreach ($opinionStatuses as $status) {
        $statusArr[$status->noun_code] = $status->toArray();
      }
      if (isset($_GET['DEBUG'])) {
        ob_start();
        echo '<h4>意見類型$typeArr:</h4>';
        var_dump($typeArr);
        echo '<h4>意見狀態$statusArr:</h4>';
        var_dump($statusArr);
      }

      // 取得目前所有提出意見紀錄
      $datas = [];
      foreach ($opinions as $opinion) {
        //notice : cmt_unit_id 與 opinion_type_id 對應有問題！
        $data = [
          'id'          => $opinion->id,
          'opinion_no'  => $opinion->opinion_no,
          'opinion'     => $opinion->opinion,
          'type_id'     => $opinion->opinion_type_id,
          'typeLabel'      => $typeArr[$opinion->opinion_type_id]['noun'],
          'opinion_status' => $opinion->opinion_status,
          'statusLabel'    => $statusArr[$opinion->opinion_status]['noun'],
          'initDateTime'=> date('Y/m/d H:i', strtotime($opinion->opinion_date)),
        ];
        $datas[] = $data;
      }

      if (isset($_GET['DEBUG'])) {
        echo '<h4>意見列表回傳結構$datas:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $datas = $err->response();
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      }else{
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }


  /**
   * GET 取得意見回覆紀錄列表
   * @todo : 加入jwt驗證
   * @param int 意見編號
   *
   * @return json
   * */
  public function record($opinionId)
  {
    $this->response->setContentType('application/json');
    try {
      if (!intval($opinionId)) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_PARAMS_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }

      // 取得單件意見內容
      $opinion = \Ifulifeapi\Models\BrandenIfullOpinion::findFirst($opinionId);
      if (!$opinion) {
        $sCode = \Personalwork\Exceptions\Exception::APPLICATION_ROWDATA_NOTFOUND;
        throw new \Personalwork\Exceptions\Exception($sCode);
      }
      $datas = $opinion->toArray();
      // 類型
      $type=\Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findFirst([
        "cmn_define_id=10 AND noun_code={$opinion->opinion_type_id}"]);
      $datas['typeLabel'] = $type->noun;
      // 狀態
      $status = \Ifulifeapi\Models\BrandenIfullCmnDefineNoun::findFirst([
        "cmn_define_id=6 AND noun_code={$opinion->opinion_status}"
      ]);
      $datas['statusLabel'] = $status->noun;

      if( count($opinion->BrandenIfullOpinionReply) ){
        $datas['replies'] = [];
        foreach($opinion->BrandenIfullOpinionReply as $reply ){
          $replyData = [
            'managerName' => $reply->BackendUsers->last_name.$reply->BackendUsers->first_name,
            'replyDateTime' => date('Y/m/d H:i', strtotime($reply->reply_date)),
            'reply' => $reply->reply,
          ];
          $datas['replies'][] = $replyData;
        }
      }

      if (isset($_GET['DEBUG'])) {
        echo '<h4>意見列表回傳結構$msgs:</h4>';
        var_dump($datas);
        $html = ob_get_contents();
        $this->response->setContent($html);
        ob_end_clean();
      }

      $this->response->setStatusCode(200, 'OK');
    } catch (\Exception $err) {
      $class = get_class($err);
      if( $class == 'Phalcon\Mvc\Model\Exception' ){
        $sCode = new \Personalwork\Exceptions\Exception(\Personalwork\Exceptions\Exception::DATABASE_PHALCON_MODEL_ERROR);
        $sCode->setMessage($err->getMessage());
        $datas = $sCode->response();
      } else {
        $datas = $err->response();
      }
      $this->response->setStatusCode($err->getCode(), $datas['codeType']);
    } finally {
      if (isset($_GET['DEBUG'])) {
        $this->response->setContent($html);
      } else {
        $this->response->setContent(json_encode($datas));
      }
      return $this->response->send();
    }
  }
}