<?php
/**
 * 處理串接Aws SNS服務發送簡訊機制
 * 1. 暫時將會員條款/隱私權政策放上html呈現
 *
 * */

namespace Ifulifeapi\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;


class PageController extends Controller
{
  /**
   * put
   * 可對特定主題取消訂閱特定Endpoint
   * @param string $topic
   * @param string $endpoint
   *
   * @return result
   * */
  public function agreement()
  {
    $this->response->setContentType('text/html');

    echo $this->view->render('agreement');
  }
}
