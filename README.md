# ifulifeapi 專案說明文件

##TODOs
- [ ] 建立SNS推播至行動裝置訊息API完整測試方案
- [ ] 邊整理第一階段開發API邊將內容轉成API文件
---

##愛富管理App
- [ ] 建立/api/backend/ prefix路由給管理端App使用
  - [ ] 登入登出機制
  - [ ] (拍照與繪圖存檔後)檔案上傳
  - [ ] 包裹收發狀態異動（收件與領件）
    - [ ] 包裹收件建檔，於之前談定的範圍是限定單筆建檔
  - [ ] 訪客登記
##愛富生活App
- [ ] 公設預約
  - [ ] 取得公設列表以及資訊
  - [ ] 依公設顯示預約紀錄
  - [ ] 預約表單
    - [ ] 進入畫面根據unitId取得即時點數
  - [ ] (**後續再做：個別用戶預約公設查詢**)單一戶別預約公設紀錄查詢
- [ ] 瓦斯抄表
  - [ ] 修正不只新增還可以修改已建立紀錄

---
###Archive:
 ✔ 建立一個撰寫API文件 @done (19-06-05 12:10) @project(工作項目 / 補充)
 ✔ 建立API串接（git協同撰寫文件） @done (19-06-05 12:10) @project(工作項目)
  1. 社區住戶開通驗證（**備註：物管人員僅使用網站系統**）
  2. 用戶註冊流程
  3. 取得/驗證碼，包含文字與QRcode
  4. 取得/檢查驗證狀態結果
  5. 傳遞/請求進行驗證資訊
  6. **傳遞/紀錄驗證流程**
 ✔ 呼叫AWS簡訊平台發送簡訊！(SnsController.php) @done (19-06-05 12:10) @project(工作項目)
 ✔ 取得回傳裝置註冊ID寫入資料庫 @done (19-06-05 12:10) @project(工作項目)
 ✔ 如何傳遞附加參數： @done(19-05-30 11:22)
  $eventManager = new \Phalcon\Events\Manager();
  ...
  $eventManager->attach( 'micro', new \Personalwork\Auth\Jwt\EventListener());
  ...
  $eventManager->fire('micro:afterExecuteRoute', $eventManager, ['aa'=>1]);
 ✔ 根據傳入className查詢的function: 使用get_class() @done (19-05-20 22:04) @project(工作項目)
 ✔ 建立/models/SystemFiles.php ORM @done (19-05-20 22:04) @project(工作項目)
 ✔ 內容關聯上傳的附加檔案存取機制 @done (19-05-20 22:04) @project(工作項目)
 ✔ models內直接取得config設定值 @done (19-05-20 22:04) @project(工作項目)
 ✔ 解決使用phalcon model建立model class問題，並測試在class內自行配置關聯測試仍然 @done (19-04-28 11:44) @project(臭蟲)
 ✔ 需要branden協助確認相關欄位 @done (19-04-28 00:47) @project(工作項目)
 ✔ Build phalcon model @done (19-04-28 00:47) @project(工作項目)