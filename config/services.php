<?php

$di = new \Phalcon\Di\FactoryDefault();

/**
 * Registering a global config
 */
$di->setShared('config', function () {

    $config = include PPS_APP_APPSPATH . "/config/config.php";

    if ( is_readable(PPS_APP_APPSPATH . '/config/config-dev.php')) {
        $override = include PPS_APP_APPSPATH . '/config/config-dev.php';
        $config->merge($override);
    }
    if ( is_readable(PPS_APP_APPSPATH . '/config/config-local.php')) {
        $override = include PPS_APP_APPSPATH . '/config/config-local.php';
        $config->merge($override);
    }

    return $config;
});


/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $dbConfig = $this->get('config')->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $db = '\Phalcon\Db\Adapter\Pdo\\' . $adapter;
    // if timezone for Asia\Taiepi
    // $db->query("SET time_zone = '+8:00';");

    return new $db($dbConfig);
});


$di->set('logger', function () {

    $logger = new \Personalwork\Logger\Adapter\Database(array(
                    'db'    => $this->get('db'),
                    'table' => "logger",
                    'name'  => 'info',
                    'router'=> $this->get('router'),
                    'request'=> $this->get('request'),
                    'session'=> $this->get('session'),
                  ));

    return $logger;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($this->get('config')->application->baseUri);
    return $url;
});

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $view = new \Phalcon\Mvc\View\Simple();
    $di = $this;
    $view->registerEngines(array(
        '.volt' => function ($view, $di){
            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions(array(
                'compiledPath' => $di->get('config')->application->cacheDir,
                'compiledSeparator' => '_',
                'stat' => true,
                'compileAlways' => true
            ));
            $compiler = $volt->getCompiler();
            $compiler->addFunction('count',
                        function($key) {
                            return "count({$key})";
                        });

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));
    $view->setViewsDir($this->get('config')->application->viewsDir);
    return $view;
});


/**
 * Starts the session the first time some component requests the session service
 */
$di->setShared('session', function () {
    $path=PPS_APP_APPSPATH."/../data/cache";
    if( !is_dir($path) ){
        @mkdir($path, 0777);
        @chmod($path, 0777);
    }
    $cachepath = realpath($path);
    // set session path
    ini_set('session.save_path', $cachepath );
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();

    return $session;
});