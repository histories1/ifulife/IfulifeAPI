<?php

if (!file_exists(PPS_APP_APPSPATH . '/vendor/autoload.php')) {
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}
require_once PPS_APP_APPSPATH . '/vendor/autoload.php';

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();
$loader
// ->registerDirs(
//     array(
//     )
// )
->registerNamespaces(
    array(
      "Ifulifeapi\\Models" => $di->get('config')->application->modelsDir,
      "Ifulifeapi\\Controllers" => $di->get('config')->application->controllersDir
    )
)
->register();